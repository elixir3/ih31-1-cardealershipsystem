﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * author   赤松克哉　
 * created  2017/11/14
 * file     DatabaseConnector.cs
 * summary  DB接続を行う
 * 
 */

namespace Ih31Azuma.Database
{
    public class DatabaseConnector
    {
        private MySqlConnection conn;
        private DataSet dataSet = null;

        public DatabaseConnector()
        {
            Connect();
        }

        private string GetKey()
        {
            return ConfigurationManager.AppSettings["DbConKey"];
        }

        private void Connect()
        {
            conn = new MySqlConnection(GetKey());
            conn.Open();
        }

        public void ExecuteNonQuery(string sql)
        {
            using (MySqlCommand cmd = new MySqlCommand(sql, conn))
            {
               cmd.ExecuteNonQuery();
            }
        }

        private void Result(string sql, string table)
        {
            dataSet = new DataSet(table);
            using (MySqlDataAdapter adapter = new MySqlDataAdapter(sql, conn))
            {
                adapter.Fill(dataSet, table);
            }
        }

        public DataSet GetDataSet(string sql, string table)
        {
            Result(sql, table);

            return dataSet;
        }

        public void Disconnect()
        {
            conn.Close();
            conn.Dispose();
        }
    }
}
