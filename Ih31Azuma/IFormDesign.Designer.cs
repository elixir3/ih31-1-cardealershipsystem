﻿namespace Ih31Azuma
{
    partial class IFormDesign
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LbStatusBar = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnF1 = new System.Windows.Forms.Button();
            this.BtnF2 = new System.Windows.Forms.Button();
            this.BtnF3 = new System.Windows.Forms.Button();
            this.BtnF4 = new System.Windows.Forms.Button();
            this.BtnF5 = new System.Windows.Forms.Button();
            this.BtnF7 = new System.Windows.Forms.Button();
            this.BtnF6 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LbStatusBar
            // 
            this.LbStatusBar.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.LbStatusBar.Font = new System.Drawing.Font("MS UI Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbStatusBar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.LbStatusBar.Location = new System.Drawing.Point(0, 450);
            this.LbStatusBar.Name = "LbStatusBar";
            this.LbStatusBar.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.LbStatusBar.Size = new System.Drawing.Size(1000, 30);
            this.LbStatusBar.TabIndex = 0;
            this.LbStatusBar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.label1.Location = new System.Drawing.Point(0, 480);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.label1.Size = new System.Drawing.Size(1000, 100);
            this.label1.TabIndex = 1;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BtnF1
            // 
            this.BtnF1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BtnF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtnF1.Location = new System.Drawing.Point(52, 491);
            this.BtnF1.Name = "BtnF1";
            this.BtnF1.Size = new System.Drawing.Size(115, 60);
            this.BtnF1.TabIndex = 2;
            this.BtnF1.Text = "button1";
            this.BtnF1.UseVisualStyleBackColor = false;
            this.BtnF1.Click += new System.EventHandler(this.BtnF1_Click);
            // 
            // BtnF2
            // 
            this.BtnF2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BtnF2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtnF2.Location = new System.Drawing.Point(182, 491);
            this.BtnF2.Name = "BtnF2";
            this.BtnF2.Size = new System.Drawing.Size(115, 60);
            this.BtnF2.TabIndex = 3;
            this.BtnF2.Text = "button2";
            this.BtnF2.UseVisualStyleBackColor = false;
            this.BtnF2.Click += new System.EventHandler(this.BtnF2_Click);
            // 
            // BtnF3
            // 
            this.BtnF3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BtnF3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtnF3.Location = new System.Drawing.Point(312, 491);
            this.BtnF3.Name = "BtnF3";
            this.BtnF3.Size = new System.Drawing.Size(115, 60);
            this.BtnF3.TabIndex = 4;
            this.BtnF3.Text = "button3";
            this.BtnF3.UseVisualStyleBackColor = false;
            this.BtnF3.Click += new System.EventHandler(this.BtnF3_Click);
            // 
            // BtnF4
            // 
            this.BtnF4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BtnF4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtnF4.Location = new System.Drawing.Point(442, 491);
            this.BtnF4.Name = "BtnF4";
            this.BtnF4.Size = new System.Drawing.Size(115, 60);
            this.BtnF4.TabIndex = 5;
            this.BtnF4.Text = "button4";
            this.BtnF4.UseVisualStyleBackColor = false;
            this.BtnF4.Click += new System.EventHandler(this.BtnF4_Click);
            // 
            // BtnF5
            // 
            this.BtnF5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BtnF5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtnF5.Location = new System.Drawing.Point(572, 491);
            this.BtnF5.Name = "BtnF5";
            this.BtnF5.Size = new System.Drawing.Size(115, 60);
            this.BtnF5.TabIndex = 6;
            this.BtnF5.Text = "button5";
            this.BtnF5.UseVisualStyleBackColor = false;
            this.BtnF5.Click += new System.EventHandler(this.BtnF5_Click);
            // 
            // BtnF7
            // 
            this.BtnF7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BtnF7.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtnF7.Location = new System.Drawing.Point(832, 491);
            this.BtnF7.Name = "BtnF7";
            this.BtnF7.Size = new System.Drawing.Size(115, 60);
            this.BtnF7.TabIndex = 7;
            this.BtnF7.Text = "button6";
            this.BtnF7.UseVisualStyleBackColor = false;
            this.BtnF7.Click += new System.EventHandler(this.BtnF7_Click);
            // 
            // BtnF6
            // 
            this.BtnF6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BtnF6.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtnF6.Location = new System.Drawing.Point(702, 491);
            this.BtnF6.Name = "BtnF6";
            this.BtnF6.Size = new System.Drawing.Size(115, 60);
            this.BtnF6.TabIndex = 8;
            this.BtnF6.Text = "btnF6";
            this.BtnF6.UseVisualStyleBackColor = false;
            this.BtnF6.Click += new System.EventHandler(this.BtnF6_Click);
            // 
            // IFormDesign
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.BtnF6);
            this.Controls.Add(this.BtnF7);
            this.Controls.Add(this.BtnF5);
            this.Controls.Add(this.BtnF4);
            this.Controls.Add(this.BtnF3);
            this.Controls.Add(this.BtnF2);
            this.Controls.Add(this.BtnF1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LbStatusBar);
            this.Name = "IFormDesign";
            this.Text = "IFormDesign";
            this.Activated += new System.EventHandler(this.Form_Activated);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FoundKeyDown);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Button BtnF1;
        protected System.Windows.Forms.Button BtnF2;
        protected System.Windows.Forms.Button BtnF3;
        protected System.Windows.Forms.Button BtnF4;
        protected System.Windows.Forms.Button BtnF5;
        protected System.Windows.Forms.Button BtnF7;
        protected System.Windows.Forms.Label LbStatusBar;
        protected System.Windows.Forms.Button BtnF6;
    }
}