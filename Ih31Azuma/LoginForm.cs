﻿using Ih31Azuma.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Ih31Azuma.MyConstant.MessageProvider;

namespace Ih31Azuma
{
    public partial class LoginForm : Form
    {
        private MainIndexForm mainIndex;
        private Configuration config;

        public LoginForm(Form parent)
        {
            mainIndex = parent as MainIndexForm;

            if (mainIndex == null)
            {
                Application.ExitThread();
            }

            InitializeComponent();
            SetFormProperty();
        }

        private void SetFormProperty()
        {
            tbPass.UseSystemPasswordChar = true;
            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            string no = config.AppSettings.Settings["User"].Value;
            if (!Check.IsEmpty(no))
            {
                tbNo.Text = no;
            }
            KeyPreview = true;
            lbStatus.Text = DialogMessage.I901LoginWelcome.GetText();
        }

        private void BtnSubmit_Click(object sender, EventArgs e)
        {
            string no = tbNo.Text;
            string pass = tbPass.Text;
            DialogMessage code;

            if (Check.IsEmpty(no))
            {
                code = DialogMessage.W904LoginNoIsEmpty;
                code.ShowMessageBox();
                lbStatus.Text = code.GetText();

                return;
            }

            LoginManager manager = new LoginManager();
            lbStatus.Text =  DialogMessage.I903LoginNow.GetText();
            if (manager.Login(no, pass))
            {
                Program.Login = true;
                Program.EmployeesNo = no;
                config.AppSettings.Settings["User"].Value = no;
                config.Save();
                mainIndex.Enabled = true;
                Close();
                Dispose();
            }
            else
            {
                code = DialogMessage.W902LoginError;
                code.ShowMessageBox();
                lbStatus.Text = code.GetText();
            }
        }

        private void LoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            mainIndex.instance = null;
            mainIndex.Activate();
        }

        private void LoginForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.F5)
            {
                BtnSubmit_Click(null, null);
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Dispose();
            Application.ExitThread();
        }
    }
}
