﻿namespace Ih31Azuma
{
    partial class MainIndexForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOrders = new System.Windows.Forms.Button();
            this.btnAccepting = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnPurchase = new System.Windows.Forms.Button();
            this.btnShipment = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCollect = new System.Windows.Forms.Button();
            this.btnCharge = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnReserve = new System.Windows.Forms.Button();
            this.btnInter = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnF1
            // 
            this.BtnF1.TabIndex = 24;
            // 
            // BtnF2
            // 
            this.BtnF2.TabIndex = 35;
            // 
            // BtnF3
            // 
            this.BtnF3.TabIndex = 46;
            // 
            // BtnF4
            // 
            this.BtnF4.TabIndex = 96;
            // 
            // BtnF5
            // 
            this.BtnF5.TabIndex = 97;
            // 
            // BtnF7
            // 
            this.BtnF7.TabIndex = 99;
            // 
            // BtnF6
            // 
            this.BtnF6.TabIndex = 98;
            // 
            // btnOrders
            // 
            this.btnOrders.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnOrders.Location = new System.Drawing.Point(205, 38);
            this.btnOrders.Name = "btnOrders";
            this.btnOrders.Size = new System.Drawing.Size(150, 45);
            this.btnOrders.TabIndex = 3;
            this.btnOrders.Text = "見積・注文";
            this.btnOrders.UseVisualStyleBackColor = true;
            this.btnOrders.Click += new System.EventHandler(this.BtnOrders_Click);
            // 
            // btnAccepting
            // 
            this.btnAccepting.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnAccepting.Location = new System.Drawing.Point(30, 38);
            this.btnAccepting.Name = "btnAccepting";
            this.btnAccepting.Size = new System.Drawing.Size(150, 45);
            this.btnAccepting.TabIndex = 2;
            this.btnAccepting.Text = "受注";
            this.btnAccepting.UseVisualStyleBackColor = true;
            this.btnAccepting.Click += new System.EventHandler(this.BtnAccepting_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnOrders);
            this.groupBox1.Controls.Add(this.btnAccepting);
            this.groupBox1.Font = new System.Drawing.Font("MS UI Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(83, 64);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(385, 110);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "受注";
            // 
            // btnPurchase
            // 
            this.btnPurchase.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnPurchase.Location = new System.Drawing.Point(30, 38);
            this.btnPurchase.Name = "btnPurchase";
            this.btnPurchase.Size = new System.Drawing.Size(150, 45);
            this.btnPurchase.TabIndex = 5;
            this.btnPurchase.Text = "仕入";
            this.btnPurchase.UseVisualStyleBackColor = true;
            this.btnPurchase.Click += new System.EventHandler(this.BtnPurchase_Click);
            // 
            // btnShipment
            // 
            this.btnShipment.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnShipment.Location = new System.Drawing.Point(205, 38);
            this.btnShipment.Name = "btnShipment";
            this.btnShipment.Size = new System.Drawing.Size(150, 45);
            this.btnShipment.TabIndex = 6;
            this.btnShipment.Text = "納品書";
            this.btnShipment.UseVisualStyleBackColor = true;
            this.btnShipment.Click += new System.EventHandler(this.BtnShipment_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnShipment);
            this.groupBox2.Controls.Add(this.btnPurchase);
            this.groupBox2.Font = new System.Drawing.Font("MS UI Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox2.Location = new System.Drawing.Point(518, 64);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(385, 110);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "仕入・出荷";
            // 
            // btnCollect
            // 
            this.btnCollect.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnCollect.Location = new System.Drawing.Point(30, 38);
            this.btnCollect.Name = "btnCollect";
            this.btnCollect.Size = new System.Drawing.Size(150, 45);
            this.btnCollect.TabIndex = 8;
            this.btnCollect.Text = "請求";
            this.btnCollect.UseVisualStyleBackColor = true;
            this.btnCollect.Click += new System.EventHandler(this.BtnCollect_Click);
            // 
            // btnCharge
            // 
            this.btnCharge.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnCharge.Location = new System.Drawing.Point(205, 38);
            this.btnCharge.Name = "btnCharge";
            this.btnCharge.Size = new System.Drawing.Size(150, 45);
            this.btnCharge.TabIndex = 9;
            this.btnCharge.Text = "入金";
            this.btnCharge.UseVisualStyleBackColor = true;
            this.btnCharge.Click += new System.EventHandler(this.BtnCharge_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnCharge);
            this.groupBox3.Controls.Add(this.btnCollect);
            this.groupBox3.Font = new System.Drawing.Font("MS UI Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox3.Location = new System.Drawing.Point(83, 213);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(385, 110);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "請求・入金";
            // 
            // btnReserve
            // 
            this.btnReserve.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnReserve.Location = new System.Drawing.Point(30, 38);
            this.btnReserve.Name = "btnReserve";
            this.btnReserve.Size = new System.Drawing.Size(150, 45);
            this.btnReserve.TabIndex = 11;
            this.btnReserve.Text = "車両予約";
            this.btnReserve.UseVisualStyleBackColor = true;
            this.btnReserve.Visible = false;
            this.btnReserve.Click += new System.EventHandler(this.BtnReserve_Click);
            // 
            // btnInter
            // 
            this.btnInter.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnInter.Location = new System.Drawing.Point(205, 38);
            this.btnInter.Name = "btnInter";
            this.btnInter.Size = new System.Drawing.Size(150, 45);
            this.btnInter.TabIndex = 12;
            this.btnInter.Text = "横流し販売";
            this.btnInter.UseVisualStyleBackColor = true;
            this.btnInter.Visible = false;
            this.btnInter.Click += new System.EventHandler(this.BtnInter_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnInter);
            this.groupBox4.Controls.Add(this.btnReserve);
            this.groupBox4.Font = new System.Drawing.Font("MS UI Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox4.Location = new System.Drawing.Point(518, 213);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(385, 110);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "仲介・予約";
            // 
            // MainIndexForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "MainIndexForm";
            this.Text = "スグクル";
            this.Controls.SetChildIndex(this.BtnF1, 0);
            this.Controls.SetChildIndex(this.BtnF2, 0);
            this.Controls.SetChildIndex(this.BtnF3, 0);
            this.Controls.SetChildIndex(this.BtnF4, 0);
            this.Controls.SetChildIndex(this.BtnF5, 0);
            this.Controls.SetChildIndex(this.BtnF7, 0);
            this.Controls.SetChildIndex(this.BtnF6, 0);
            this.Controls.SetChildIndex(this.LbStatusBar, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.groupBox2, 0);
            this.Controls.SetChildIndex(this.groupBox3, 0);
            this.Controls.SetChildIndex(this.groupBox4, 0);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnOrders;
        private System.Windows.Forms.Button btnAccepting;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnPurchase;
        private System.Windows.Forms.Button btnShipment;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnCollect;
        private System.Windows.Forms.Button btnCharge;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnReserve;
        private System.Windows.Forms.Button btnInter;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}