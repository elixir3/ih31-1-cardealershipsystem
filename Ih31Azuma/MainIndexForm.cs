﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Ih31Azuma.MyConstant.MessageProvider;

namespace Ih31Azuma
{
    public partial class MainIndexForm : IFormDesign
    {
        private string btnF1Text = "受注 (F1)";
        private string btnF2Text = "見積・注文(F2)";
        private string btnF3Text = "仕入 (F3)";
        private string btnF4Text = "出荷 (F4)";
        private string btnF5Text = "請求 (F5)";
        private string btnF6Text = "回収 (F6)";

        public MainIndexForm()
        {
            InitializeComponent();
            BtnF1.Text = btnF1Text;
            BtnF2.Text = btnF2Text;
            BtnF3.Text = btnF3Text;
            BtnF4.Text = btnF4Text;
            BtnF5.Text = btnF5Text;
            BtnF6.Text = btnF6Text;

            groupBox1.TabStop = false;
            groupBox2.TabStop = false;
            groupBox3.TabStop = false;
            groupBox4.TabStop = false;
        }

        protected override void SetFunctionKey()
        {
            base.SetFunctionKey();
            EnableButton(BtnF1, btnF1Text);
            EnableButton(BtnF2, btnF2Text);
            EnableButton(BtnF3, btnF3Text);
            EnableButton(BtnF4, btnF4Text);
            EnableButton(BtnF5, btnF5Text);
            EnableButton(BtnF6, btnF6Text);
        }

        protected override void BtnF1_Click(object sender = null, EventArgs e = null)
        {
            OrdersAcceptingForm form = new OrdersAcceptingForm();
            form.Show();
        }

        protected override void BtnF2_Click(object sender = null, EventArgs e = null)
        {
            //ここを変えたぞー！！！ジョジョーーー！！
            OrdersListSearchForm form = new OrdersListSearchForm();
            form.Show();
        }

        protected override void BtnF3_Click(object sender = null, EventArgs e = null)
        {
            PurchaseForm form = new PurchaseForm();
            form.Show();
        }

        protected override void BtnF4_Click(object sender = null, EventArgs e = null)
        {
            ShipmentForm form = new ShipmentForm();
            form.Show();
        }

        protected override void BtnF5_Click(object sender = null, EventArgs e = null)
        {
            ChargeForm form = new ChargeForm();
            form.Show();
        }

        protected override void BtnF6_Click(object sender = null, EventArgs e = null)
        {
            CollectForm form = new CollectForm();
            form.Show();
        }

        protected override void BtnF7_Click(object sender = null, EventArgs e = null)
        {
            DialogResult result = MessageBox.Show("終了しますか？", "確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result == DialogResult.OK)
            {
                Close();
                Dispose();
                Application.ExitThread();
            }
        }

        private void BtnAccepting_Click(object sender, EventArgs e)
        {
            BtnF1_Click();
        }

        private void BtnOrders_Click(object sender, EventArgs e)
        {
            BtnF2_Click();
        }

        private void BtnPurchase_Click(object sender, EventArgs e)
        {
            BtnF3_Click();
        }

        private void BtnShipment_Click(object sender, EventArgs e)
        {
            BtnF4_Click();
        }

        private void BtnCharge_Click(object sender, EventArgs e)
        {
            BtnF5_Click();
        }

        private void BtnCollect_Click(object sender, EventArgs e)
        {
            BtnF6_Click();
        }

        private void BtnReserve_Click(object sender, EventArgs e)
        {
            MessageBox.Show("工事中");
        }

        private void BtnInter_Click(object sender, EventArgs e)
        {
            MessageBox.Show("工事中");
        }
    }
}
