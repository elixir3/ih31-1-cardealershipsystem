﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
 * author   赤松克哉　
 * created  2017/11/20
 * file     ConstantProvider.cs
 * summary  CSVから定数を読み込む
 * 
 */

namespace Ih31Azuma.MyConstant
{
    class ConstantProvider
    {
        private List<Tax> TaxArray; //消費税に関するデータが格納されている

        private string currentDirectory; //現在のディレクトリを格納しておく
       
        public ConstantProvider()
        {
            Load();
        }

        public int GetTaxValue(DateTime dateTime)
        {
            foreach (Tax tax in TaxArray)
            {
                if (tax.Start <= dateTime && tax.End >= dateTime)
                {
                    return tax.Value;
                }
            }

            return 0;
        }

        public int GetTaxValue()
        {
            return GetTaxValue(DateTime.Today);
        }

        public int CalcIncludeTax(int price, DateTime dateTime)
        {
            double tax = (double)GetTaxValue(dateTime) / 100;
            return (int)(price * (1 + tax));
        }

        public int CalcIncludeTax(int price)
        {
            return CalcIncludeTax(price, DateTime.Today);
        }

        /*
         * まとめてロードを行う
         */
        private void Load()
        {
            currentDirectory = Environment.CurrentDirectory; //現在のディレクトリを格納する
            LoadTax(); //tax.csvのロード開始
        }

        /**
         * tax.csvの読み込みを行いListに格納する
         */
        private void LoadTax()
        {
            string filePath = currentDirectory + "\\tax.csv"; //ファイル名
            TaxArray = new List<Tax>();
            Tax tax; 
            string[] line;
            int value = 0;
            DateTime date;
            using (StreamReader sr = new StreamReader(filePath))
            {
                while (!sr.EndOfStream)
                {
                    tax = new Tax();
                    line = new string[3];
                    line = sr.ReadLine().Split(',');
                    Int32.TryParse(line[0], out value);
                    tax.Value = value;
                    DateTime.TryParse(line[1], out date);
                    tax.Start = date;
                    DateTime.TryParse(line[2], out date);
                    tax.End = date;
                    TaxArray.Add(tax);
                }
            }
        }

        /**
         * 消費税に関するデータをまとめるクラス
         */
        protected class Tax
        {
            public int Value { set; get; }
            public DateTime Start { set; get; }
            public DateTime End { set; get; }
        }
    }
}
