﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
 * author   赤松克哉　
 * created  2017/11/14
 * file     DialogMessage.cs
 * summary  メッセージデータを格納し出力する
 * 
 */

namespace Ih31Azuma.MyConstant
{
    public static class MessageProvider
    {
        public enum DialogMessage //メッセージリスト一覧（連番）
        {
            I001OrdersAcceptingConfirm, //0
            I002OrdersAcceptingRegistered, //1
            W003OrdersNameIsEmpty, //2
            W004OrdersNotExsitName,
            W005OrdersCarIsEmpty,
            W006OrdersQuantityIsEmpty,
            W007OrdersQuantityIsNotHalfWidth,
            W008OrdersBudgetIsEmpty,
            W009OrdersBudgetIsNotHalfWidth,
            W010OrdersDeadlineIsEmpty,
            W011OrdersDeadlineIsNotHalfWidth,
            I012OrdersQuotationConfirm,
            I013OrdersQuotationRegistered,
            I014OrdersPurchaseConfirm,
            I015OrdersPurchaseRegistered,
            E016OrdersAcceptingNotRegistered,
            E017OrdersQuotationNotRegistered,
            E018OrdersPurchaseNotRegistered,
            W019SearchNotExsitName,
            W020SearchNotExsitPurchaseNo,
            W021SearchNotExsitOrdersNo,
            W022DeliverNotExsitName,
            W501PurchaseNotExsitAcceptingNo,
            W502PurchaseAcceptingNoIsEmpty,
            W503PurchaseCarNameIsEmpty,
            W504PurchaseSupplierIsEmpty,
            W505PurchasePriceIsEmpty,
            W506PurchaseBudgetIsEmpty,
            I507ReserveReserved,
            W508InterNotExistAcceptingNo,
            W509InterAcceptingNoIsEmpty,
            W510InterNotExistAcceptingNo,
            W511InterNotExsistCustomerName,
            W512InterNotSelectCar,
            W513ChargeNotExsistName,
            W514ChargeDateIsEmpty,
            W515ChargeNotMatchDeadline,
            W516ChargeNotExsistName,
            I517ChargeConfirm,
            W518CollectNotExsistName,
            W523CollectDateIsEmpty,
            W524CollectNotMatchDeadline,
            W526CollectNotExsistName,
            I527CollectCollected,
            I901LoginWelcome,
            W902LoginError,
            I903LoginNow,
            W904LoginNoIsEmpty,
            W905ShipmentNoIsEmpty,
            W906ShipmentNoIsWrong,
            W907ShipmentDataIsWrong,
            I908CollectComp
        }
        private static readonly string[] messageList = { //メッセージテキストデータ
           "登録します。よろしいですか",
           "登録完了しました",
            "顧客名を入力されていません",
            "一致する顧客名がありません",
            "車両名を入力されていません",
            "台数を入力されていません",
            "台数は半角数字で入力されていません",
            "予算を入力されていません",
            "予算は半角数字で入力されていません",
            "納期を入力されていません",
            "納期は半角数字で入力されていません",
            "作成します。よろしいですか",
            "作成完了しました。",
            "作成します。よろしいですか",
            "作成完了しました。",
            "登録できません",
            "作成できません",
            "作成できません",
            "一致する顧客名がありません",
            "一致する仕入番号がありません",
            "一致する受注番号がありません",
            "顧客名を選択されていません",
            "一致する受注番号はありません。",
            "受注番号が入力されていません。",
            "車名が選択されていません。",
            "仕入先が入力されていません。",
            "仕入金額が入力されていません。",
            "経費が入力されていません。",
            "予約完了しました。",
            "一致する受注書番号はありません。",
            "受注書番号が入力されていません。",
            "一致する受注番号はありません。",
            "一致する顧客名はありません。",
            "車名が選択されていません。",
            "一致する顧客名はありません。",
            "請求月が入力されていません。",
            "一致する納期はありません。",
            "一致する顧客名はありません。",
            "請求しますよろしいですか。",
            "一致する顧客名はありません。",
            "請求月が入力されていません。",
            "一致する納期はありません。",
            "一致する顧客名はありません。",
            "入金完了しました。",
            "ログインしてください。",
            "社員番号とパスワードが一致しません。",
            "ログイン中",
            "社員番号が入力されていません。",
            "番号が入力されていません。",
            "一致する番号はありません。",
            "納品日の形式が正しくありません",
            "入金処理が完了しました。"
        };

        public static string GetText(this DialogMessage message)
        {
            return messageList[Convert.ToInt32(message)];
        }

        public static string GetText(DialogMessage[] messages)
        {
            StringBuilder sb = new StringBuilder();

            for (int index = 0; index < messages.Length; index++)
            {
                if (index != 0)
                {
                    sb.Append("　");
                }
                sb.Append(messageList[Convert.ToInt32(messages[index])]);
            }
            return sb.ToString();
        }

        public static string GetText(List<DialogMessage> list)
        {
            DialogMessage[] messages = list.ToArray();
            return GetText(messages);
        }

        public static void ShowMessageBox(this DialogMessage message)
        {
            MessageBoxParmeter param = GetMesssgeBoxData(message);
            MessageBox.Show(messageList[Convert.ToInt32(message)], param.Caption, param.Button, param.Icon);
        }

        public static void ShowMessageBox(this DialogMessage message, MessageBoxButtons button)
        {
            MessageBoxParmeter param = GetMesssgeBoxData(message);
            MessageBox.Show(messageList[Convert.ToInt32(message)], param.Caption, button, param.Icon);
        }

        private static MessageBoxParmeter GetMesssgeBoxData(this DialogMessage message)
        {
            string code = message.ToString().Substring(0, 1);
            return GetParameter(code);
        }

        public static void ShowMessageBox(DialogMessage[] messages)
        {
            MessageBoxParmeter param = null;
            StringBuilder sb = new StringBuilder();
            for (int index = 0; index < messages.Length; index++)
            {
                if (index == 0)
                {
                    param = GetMesssgeBoxData(messages[0]);
                }
                else
                {
                    sb.Append(Environment.NewLine);
                }
                sb.Append(messageList[Convert.ToInt32(messages[index])]);
            }
            MessageBox.Show(sb.ToString(), param.Caption, param.Button, param.Icon);

        }

        public static void ShowMessageBox(List<DialogMessage> list)
        {
            DialogMessage[] messages = list.ToArray();
            ShowMessageBox(messages);
        }

        public static void ShowMessageBox(DialogMessage[] messages, MessageBoxButtons button)
        {
            MessageBoxParmeter param = null;
            StringBuilder sb = new StringBuilder();
            for (int index = 0; index < messages.Length; index++)
            {
                if (index == 0)
                {
                    param = GetMesssgeBoxData(messages[0]);
                }
                else
                {
                    sb.Append(Environment.NewLine);
                }
                sb.Append(messageList[Convert.ToInt32(messages[index])]);
            }
            MessageBox.Show(sb.ToString(), param.Caption, button, param.Icon);

        }

        public static void ShowMessageBox(List<DialogMessage> list, MessageBoxButtons button)
        {
            DialogMessage[] messages = list.ToArray();
            ShowMessageBox(messages, button);
        }

        private static MessageBoxParmeter GetParameter(string code)
        {
            const string error = "E";
            const string infomation = "I";
            const string waring = "W";
            MessageBoxParmeter param = new MessageBoxParmeter();
            switch (code)
            {
                case error:
                    param.Caption = "致命的なエラー";
                    param.Button = MessageBoxButtons.OK;
                    param.Icon = MessageBoxIcon.Error;
                    break;
                case infomation:
                    param.Caption = "メッセージ";
                    param.Button = MessageBoxButtons.YesNo;
                    param.Icon = MessageBoxIcon.Information;
                    break;
                case waring:
                    param.Caption = "エラー";
                    param.Button = MessageBoxButtons.OK;
                    param.Icon = MessageBoxIcon.Warning;
                    break;
                default:
                    break;
            }

            return param;
        }

        protected class MessageBoxParmeter
        {
            internal string Caption { get; set; }
            internal MessageBoxButtons Button { get; set; }
            internal MessageBoxIcon Icon { get; set; }
        }
    }
}
