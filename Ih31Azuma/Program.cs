﻿using Ih31Azuma.MyConstant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ih31Azuma
{
    static class Program
    {
        public static ConstantProvider ConstantProvider; //定数クラスのインスタンス
        public static bool Login = false;
        public static string EmployeesNo;
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ConstantProvider = new ConstantProvider(); // 定数クラスのオブジェクトを生成
            Application.Run(new MainIndexForm());
        }
    }
}
