﻿using Ih31Azuma.MyConstant;
using Ih31Azuma.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Ih31Azuma.MyConstant.ConstantProvider;
using static Ih31Azuma.MyConstant.MessageProvider;

namespace Ih31Azuma
{
    public partial class ReserveCarForm : IFormDesign
    {
        public ReserveCarForm()
        {
            InitializeComponent();
            insertDgvList();

            //MessageBox.Show(Program.ConstantProvider.CalcIncludeTax(100).ToString());
        }

        private void insertDgvList()
        {
            List<ReserveCar> list = sampleData();
            ReserveCar car;

            for (int cnt = 0; cnt < list.Count; cnt ++)
            {
                car = list[cnt];
                this.dgvList.Rows.Add(car.Id, car.Status, car.Name);

                if (car.Status.Equals("予約済"))
                {
                    this.dgvList.Rows[cnt].DefaultCellStyle.BackColor = Color.MistyRose;
                }
                else if(car.Status.Equals("予約中"))
                {
                    this.dgvList.Rows[cnt].DefaultCellStyle.BackColor = Color.LightCyan;
                }

            }
        }

        private List<ReserveCar> sampleData()
        {
            List<ReserveCar> list = new List<ReserveCar>();

            ReserveCar car = new ReserveCar();
            car.Id = "1";
            car.Status = "予約済";
            car.Name = "谷";
            list.Add(car);

            car = new ReserveCar();
            car.Id = "2";
            car.Status = "予約済";
            car.Name = "まさる";
            list.Add(car);

            car = new ReserveCar();
            car.Id = "3";
            car.Status = "";
            car.Name = "";
            list.Add(car);

            return list;
        }

        private class ReserveCar
        {
            private string _id;
            private string status;
            private string name;

            public string Id
            {
                set
                {
                    this._id = value;
                }
                get
                {
                    return this._id;
                }
            }

            public string Status
            {
                set
                {
                    this.status = value;
                }
                get
                {
                    return this.status;
                }
            }

            public string Name
            {
                set
                {
                    this.name = value;
                }
                get
                {
                    return this.name;
                }
            }
        }
    }
}
