﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
 * author   赤松克哉　
 * created  2017/11/20
 * file     Check.cs
 * summary  数値チェックを行う
 * 
 */

namespace Ih31Azuma.Util
{
    class Check
    {
        public static bool IsEmpty(string s)
        {
            if (s == null || s.Equals(""))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsZipCode(string s)
        {
            if (s.Length == 7)
            {
                return IsNumeric(s);
            }
            else
            {
                return false;
            }
        }

        public static bool IsNumeric(string s)
        {
            return int.TryParse(s, out int _);
        }

        public static bool IsExceptionKeys(char e)
        {
            if (e == (char)Keys.Back || e == (char)Keys.Escape || e == (char)Keys.Enter)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsFormNo(string s)
        {
            if(s.Length == 6)
            {
                return IsNumeric(s.Substring(1,6));
            }
            else if (s.Length == 5)
            {
                return IsNumeric(s);
            }
            else
            {
                return false;
            }
        }
    }
}
