﻿using Ih31Azuma.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ih31Azuma.Util
{
    public class LoginManager
    {
        private const string key = "masaru_tani";

        private string GetHashText(string password)
        {
            byte[] passByte = Encoding.UTF8.GetBytes(password);
            byte[] keyByte = Encoding.UTF8.GetBytes(key);

            HMACSHA256 sha = new HMACSHA256(keyByte);
            byte[] hash = sha.ComputeHash(passByte);

            return BitConverter.ToString(hash).Replace("-","");
        }

        public bool Login(string no, string password)
        {
            string table = "employees";
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT Employees_no ");
            sql.Append("FROM "+ table +" WHERE ");
            sql.Append("Employees_no =\"" + no + "\" ");
            sql.Append("AND Employees_password = \"" + GetHashText(password) + "\"");
            DatabaseConnector conn = new DatabaseConnector();
            DataSet dataSet = conn.GetDataSet(sql.ToString(), table);

            if (dataSet.Tables[table].Rows.Count == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
