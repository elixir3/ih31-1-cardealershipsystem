﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ih31Azuma.Util
{
    class MyConvert
    {
        public static string ConvetShipmentNo(string s)
        {
            if(!int.TryParse(s, out int no))
            {
                Error();
            }

            if (s.Length <= 5)
            {
                return (400000 + no).ToString();
            }
            else if (s.Length == 6 && s.Substring(0, 1) == "4")
            {
                return s;
            }

            Error();

            return null;
        }

        private static void Error()
        {
            throw new FormatException();
        }

        public static string AddComma(string price, bool add)
        {
            if (int.TryParse(price, out int i))
            {
                if (add)
                {
                    return i.ToString("#,0");
                }

                return price;

            }
            else
            {
                if (!add)
                {
                    return price.Replace(",", "");
                }

                return null;
            }
            
        }

        public static string ConvertDeadLine(string date)
        {
           if(DateTime.TryParse(date, out DateTime d))
            {
                int year = d.Year;
                int month = d.Month;
                int day = d.Day;
                if (day > 20)
                {
                    year = (month == 12) ? year++ : year;
                    month = (month == 12) ? 1 : month++;
                }

                return year + "/" + month + "/" + 25;
            }
            else
            {
                Error();
                return null;
            }
        }

        public static string ConvetPurchaseNo(string s)
        {
            if (!int.TryParse(s, out int no))
            {
                Error();
            }

            if (s.Length <= 5)
            {
                return (300000 + no).ToString();
            }
            else if (s.Length == 6 && s.Substring(0, 1) == "3")
            {
                return s;
            }

            Error();

            return null;
        }
    }
}
