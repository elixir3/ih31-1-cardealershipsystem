﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Ih31Azuma.Util
{
    public class PrintPdf
    {
        public static void JoinPdf(int count, string path)
        {
            PdfReader reader;
            using (Document doc = new Document())
            {
                using (PdfCopy copy = new PdfCopy(doc, new FileStream(path + "exports.pdf", FileMode.Create)))
                {
                    doc.Open();
                    for(int index = 0; index < count; index++)
                    {
                        reader = new PdfReader(path + index + ".pdf");
                        copy.AddDocument(reader);
                        reader.Close();
                        reader.Dispose();
                    }
                }
            }
        }

        public static void JoinPdf(List<string> list, string path)
        {
            JoinPdf(list.Count, path);
        }

        public static void EndPrint(string path)
        {
            DirectoryInfo directory = new DirectoryInfo(path);
            if (directory.Exists)
            {
                foreach (FileInfo file in directory.GetFiles())
                {
                    file.Delete();
                }
                directory.Delete();
            }

        }
    }
}
