﻿using Ih31Azuma._akamatsu;
using Ih31Azuma.Database;
using Ih31Azuma.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Ih31Azuma.MyConstant.MessageProvider;
using static Ih31Azuma.ShipmentPrint;

namespace Ih31Azuma
{
    public partial class ShipmentForm : IFormDesign
    {
        private DataSet dataSet;
        string requirements;

        public ShipmentForm()
        {
            InitializeComponent();
            EnableDataGridViewOperator();
        }

        private void FoundKeyPress(object sender, KeyPressEventArgs e)
        {
            if (Check.IsExceptionKeys(e.KeyChar) 
                || Check.IsNumeric(e.KeyChar.ToString()) 
                || e.KeyChar == '/'
                || e.KeyChar == '-')
            {
                return;
            }

            e.Handled = true;
        }

        protected override void FoundKeyDown(object sender, KeyEventArgs e)
        {
            base.FoundKeyDown(sender, e);
            if (e.KeyCode == Keys.Enter)
            {
                if (dataGridView1.Rows.Count != 0)
                {
                    CellDoubleClick(null, null);
                }  
            }
        }

        protected override void SetFunctionKey()
        {
            base.SetFunctionKey();
            EnableButton(BtnF1, "検索 (F1)");
            EnableButton(BtnF2, "印刷 (F2)");
        }

        protected override void BtnF1_Click(object sender = null, EventArgs e = null)
        {
            Submit();
            if (requirements == null)
            {
                return;
            }

            GetShipmentList(requirements);
        }

        protected override void BtnF2_Click(object sender = null, EventArgs e = null)
        {
            if(dataGridView1.Rows.Count != 0)
            {
                Print();
            }
        }

        protected override void BtnF3_Click(object sender = null, EventArgs e = null)
        {
            MoveSelectRow(Keys.F3, dataGridView1);
        }

        protected override void BtnF4_Click(object sender = null, EventArgs e = null)
        {
            MoveSelectRow(Keys.F4, dataGridView1);
        }

        protected override void BtnF6_Click(object sender = null, EventArgs e = null)
        {
            base.BtnF6_Click(sender, e);
        }

        private void Submit()
        {
            requirements = null;

            if (Check.IsEmpty(tbNo.Text))
            {
                DialogMessage.W905ShipmentNoIsEmpty.ShowMessageBox();
                return;
            }

            string[] noArray = tbNo.Text.Split('/');
            string[] subArray;
            StringBuilder sb = new StringBuilder();

            try
            {
                bool is1st = true;
                foreach (string no in noArray)
                {
                    if (is1st)
                    {
                        sb.Append(" WHERE ");
                        is1st = false;
                    }
                    else
                    {
                        sb.Append(" OR ");
                    }

                    subArray = no.Split('-');

                    if (subArray.Length == 2)
                    {
                        sb.Append("shipping_no >= " + MyConvert.ConvetShipmentNo(subArray[0]));
                        sb.Append(" AND shipping_no <= " + MyConvert.ConvetShipmentNo(subArray[1]));
                    }
                    else
                    {
                        sb.Append("shipping_no = " + MyConvert.ConvetShipmentNo(subArray[0]));
                    }
                }
            }
            catch (FormatException)
            {
                DialogMessage.W906ShipmentNoIsWrong.ShowMessageBox();
                return;
            }

            requirements = sb.ToString();
        }

        private void GetShipmentList(string requirements)
        {
            string table = "shipment_list";
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT `shipping_no`, `customers_name`, `orderreceive_deliverydate`, `shipping_making_date` ");
            sql.Append("FROM `shipping` ");
            sql.Append("INNER JOIN `orderreceive` ");
            sql.Append("ON `orderreceive_no` = `shipping_orders_no` ");
            sql.Append("INNER JOIN customers ");
            sql.Append("ON orderreceive_client_no  = customers_no ");
            sql.Append(" AND orderreceive_serialnumber = 1 ");
            sql.Append(requirements);
            Clipboard.SetText(sql.ToString());
            DatabaseConnector conn = new DatabaseConnector();
            dataSet = conn.GetDataSet(sql.ToString(), table);
            conn.Disconnect();

            SetDataGridView(table);
        }

        private void SetDataGridView(string table)
        {
            DataTable dataTble = dataSet.Tables[table];
            dataGridView1.Columns.Clear();
            checkBox1.Checked = false;

            if (dataTble.Rows.Count != 0)
            {
                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                dataGridView1.Columns.Add(check);
                dataGridView1.DataSource = dataTble;
                SetDataGridViewProperty();
                tbDate.Enabled = true;
                tbDate.Text = DateTime.Today.ToString("yyyy/MM/dd");
                dataGridView1.Rows[0].Selected = true;
                dataGridView1.CurrentCell = dataGridView1[0,0];
            }
            else
            {
                tbDate.Enabled = false;
                DialogMessage.W906ShipmentNoIsWrong.ShowMessageBox();
            }
        }

        private void SetDataGridViewProperty()
        {
            string[] headerText = new string[] { "", "納品書番号", "顧客名", "納期", "出荷日"};

            for (int count = 0; count < headerText.Length; count++)
            {
                if (Check.IsEmpty(headerText[count]))
                {
                    dataGridView1.Columns[count].ReadOnly = false;
                }
                else
                {
                    dataGridView1.Columns[count].ReadOnly = true;
                }
                dataGridView1.Columns[count].HeaderText = headerText[count];
            }


            int width = dataGridView1.Size.Width;
            int scrollBar = 17;
            if (dataGridView1.Rows.Count > 25)
            {
                width = width - scrollBar;
            }

            //var center = DataGridViewContentAlignment.MiddleCenter; //中央揃え
            //var right = DataGridViewContentAlignment.MiddleRight; //右揃え

            dataGridView1.Columns[0].Width = 50;
            dataGridView1.Columns[2].Width = 150;
            dataGridView1.Columns[3].Width = 150;
            dataGridView1.Columns[4].Width = 150;

            dataGridView1.Columns[1].Width = width - (150*3 +50 +3);
        }

        private void CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = dataGridView1.CurrentRow.Index;

            if (dataGridView1.Rows[index].Cells[0].Value is true)
            {
                dataGridView1.Rows[index].Cells[0].Value = false;
            }
            else if(dataGridView1.Rows[index].Cells[0].Value is false || dataGridView1.Rows[index].Cells[0].Value is null)
            {
                dataGridView1.Rows[index].Cells[0].Value = true;
            }

            BtnF4_Click();
        }

        private void CheckedChanged(object sender, EventArgs e)
        {
            bool isChecked = checkBox1.Checked;
            if (dataGridView1.Rows.Count != 0)
            {
                for (int index = 0; index < dataGridView1.Rows.Count; index++)
                {
                    dataGridView1.Rows[index].Cells[0].Value = isChecked;
                }
            }

        }

        private void Print()
        {
            List<string> list = new List<string>();
            SearchCheckedRows(list);

            if (list.Count is 0)
            {
                list.Add(dataGridView1.CurrentRow.Cells[1].Value.ToString());
            }

            DateTime deadline;

            if (!DateTime.TryParse(tbDate.Text, out deadline))
            {
                DialogMessage.W907ShipmentDataIsWrong.ShowMessageBox();
                return;
            }

            string mainTable = "shipment_data";
            StringBuilder main = new StringBuilder();
            main.Append("SELECT orderreceive_serialnumber, orderreceive_carname, purchase_car_number, ");
            main.Append("purchase_price, purchase_expenses, (purchase_price +  purchase_expenses) as sum_price ");
            main.Append("FROM " + mainTable);

            string subTable = "shipment_strings";
            StringBuilder sub = new StringBuilder();
            sub.Append("SELECT sum(purchase_price) as purchase_price, sum(purchase_expenses) as ex_price, customers_name ");
            sub.Append(" FROM " + mainTable);
            sub.Append(" INNER JOIN orderreceive ON shipment_data.orderreceive_no = orderreceive.orderreceive_no ");
            sub.Append(" AND shipment_data.orderreceive_serialnumber = orderreceive.orderreceive_serialnumber ");
            sub.Append(" INNER JOIN customers ON orderreceive_client_no = customers_no  ");

            DatabaseConnector conn = new DatabaseConnector();
            DataSet dataSet;
            DataSet subDataSet;
            string mainStr = main.ToString();
            string subStr = sub.ToString();
            ShipmentPrint print;
            ShipmentStrings strings;
            DataTable dataTable;
            string price;
            bool showPdfViewer = false;
            int no;

            for(int count = 0; count < list.Count; count++)
            {
                no = Convert.ToInt32(list[count]) % 400000;
                dataSet = conn.GetDataSet(
                    mainStr + " WHERE orderreceive_no = " + Convert.ToInt32(list[count])%400000,mainTable);
                subDataSet = conn.GetDataSet(
                    subStr 
                    + " WHERE shipment_data.orderreceive_no = "
                    + no
                    + " GROUP BY shipment_data.orderreceive_no"
                    , subTable);
                dataTable = subDataSet.Tables[subTable];
                strings = new ShipmentStrings();
                strings.ShipmentNo = list[count];
                price = dataTable.Rows[0][0].ToString();
                strings.Price = (Program.ConstantProvider.CalcIncludeTax(Convert.ToInt32(price))
                    + Convert.ToInt32(dataTable.Rows[0][1].ToString())).ToString();
                strings.CustomerName = dataTable.Rows[0][2].ToString();
                strings.DeadLine = tbDate.Text;

                InsertCollect(conn, no, strings);

                if (list.Count == 1)
                {
                    print = new ShipmentPrint(dataSet, strings);
                    print.Show();
                }
                else
                {
                    if (count == 0)
                    {
                        showPdfViewer = true;
                    }
                    print = new ShipmentPrint(dataSet, strings, count.ToString());
                    print.Dispose();
                }
            }

            conn.Disconnect();

            if (showPdfViewer)
            {
                PrintPdf.JoinPdf(list, "shipment/");
                ShipmentPdfPrint form = new ShipmentPdfPrint();
                form.Show();
            }

            GetShipmentList(requirements);
        }

        private void InsertCollect(DatabaseConnector conn, int no, ShipmentStrings strings)
        {
            string check = "select 1 from collect where collect_order_no = " + no;
            string table = "collect";
            StringBuilder sql = new StringBuilder();
            DataSet dSet = conn.GetDataSet(check, table);

            if (dSet.Tables[table].Rows.Count == 0)
            {
                sql.Append("UPDATE `shipping` SET ");
                sql.Append("`shipping_making_date` =\"" + strings.DeadLine + "\"");
                sql.Append(" WHERE `shipping_no` =" + strings.ShipmentNo);
                conn.ExecuteNonQuery(sql.ToString());
                sql.Clear();

                sql.Append("INSERT INTO `collect`( ");
                sql.Append("`collect_no`, `collect_order_no`, `collect_closing_date`, `collect_price`, `collect_insert_date`, `collect_insert_time` ");
                sql.Append(") VALUES (");
                sql.Append((900000 + no) + ", ");
                sql.Append(no + ",");
                sql.Append("\"" + MyConvert.ConvertDeadLine(strings.DeadLine) + "\",");
                sql.Append(MyConvert.AddComma(strings.Price, false) + ",");
                sql.Append("now(), now())");
                conn.ExecuteNonQuery(sql.ToString());
                sql.Clear();

                sql.Append("UPDATE orderreceive SET ");
                sql.Append("orderreceive_status = 4 ");
                sql.Append("WHERE orderreceive_no = " + no);
                conn.ExecuteNonQuery(sql.ToString());

            }
        }

        private void SearchCheckedRows(List<string> list)
        {
            for (int index = 0; index < dataGridView1.Rows.Count; index++)
            {
                if (dataGridView1.Rows[index].Cells[0].Value is true)
                {
                    list.Add(dataGridView1.Rows[index].Cells[1].Value.ToString());
                }
            }
        }
    }
}
