﻿using Ih31Azuma.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ih31Azuma._akamatsu
{
    public partial class ShipmentPdfPrint : IFormDesign
    {
        public ShipmentPdfPrint()
        {
            InitializeComponent();
            axAcroPDF1.LoadFile(@"shipment/exports.pdf");
        }

        private void ShipmentPdfPrint_Load(object sender, EventArgs e)
        {
            axAcroPDF1.Refresh();
        }

        protected override void SetFunctionKey()
        {
            base.SetFunctionKey();
            EnableButton(BtnF2, "印刷");
        }

        protected override void BtnF2_Click(object sender = null, EventArgs e = null)
        {
            axAcroPDF1.printWithDialog();
        }

        private void ShipmentPdfPrint_FormClosed(object sender, FormClosedEventArgs e)
        {
            PrintPdf.EndPrint("shipment");
        }
    }
}
