﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Ih31Azuma._akamatsu;
using Ih31Azuma.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ih31Azuma
{
    public partial class ShipmentPrint : IFormDesign
    {
        private DataSet dataSet;
        private ShipmentStrings strings;

        public ShipmentPrint(DataSet dataSet, ShipmentStrings strings)
        {
            InitializeComponent();
            this.dataSet = dataSet;
            this.strings = strings;
        }

        public ShipmentPrint(DataSet dataSet, ShipmentStrings strings, string line)
        {
            InitializeComponent();
            this.dataSet = dataSet;
            this.strings = strings;
            CreateFolder();
            ExportPdf(line);
        }

        private void CreateFolder()
        {
            string path = "shipment";
            if (!Directory.Exists(@path))
            {
                Directory.CreateDirectory(@path);
            }
        }

        private void ExportPdf(string line)
        {

            ShipmentReports1.SetDataSource(dataSet);
            TextObject txNo = (TextObject)ShipmentReports1.ReportDefinition.ReportObjects["txNo"];
            txNo.Text = strings.ShipmentNo;
            TextObject txDate = (TextObject)ShipmentReports1.ReportDefinition.ReportObjects["txDate"];
            txDate.Text = strings.DeadLine;
            TextObject txCustomersName = (TextObject)ShipmentReports1.ReportDefinition.ReportObjects["txCustomersName"];
            txCustomersName.Text = strings.CustomerName;
            TextObject tbSum = (TextObject)ShipmentReports1.ReportDefinition.ReportObjects["tbSum"];
            tbSum.Text = strings.Price;

            ShipmentReports1.Refresh();

            DiskFileDestinationOptions disk = new DiskFileDestinationOptions();
            disk.DiskFileName = "shipment/" + line + ".pdf";
            ExportOptions opt = new ExportOptions();
            opt = ShipmentReports1.ExportOptions;
            opt.ExportDestinationType = ExportDestinationType.DiskFile;
            opt.ExportFormatType = ExportFormatType.PortableDocFormat;
            opt.ExportFormatOptions = new PdfRtfWordFormatOptions();
            opt.ExportDestinationOptions = disk;

            ShipmentReports1.Export();
        }

        private void CrystalReportViewer1_Load(object sender, EventArgs e)
        {
            crystalReportViewer1.ReportSource = InsertReport();
            crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
        }

        public ShipmentReports InsertReport()
        {
            ShipmentReports reports = new ShipmentReports();
            reports.SetDataSource(dataSet);

            TextObject txNo = (TextObject)reports.ReportDefinition.ReportObjects["txNo"];
            txNo.Text = strings.ShipmentNo;
            TextObject txDate = (TextObject)reports.ReportDefinition.ReportObjects["txDate"];
            txDate.Text = strings.DeadLine;
            TextObject txCustomersName = (TextObject)reports.ReportDefinition.ReportObjects["txCustomersName"];
            txCustomersName.Text = strings.CustomerName;
            TextObject tbSum = (TextObject)reports.ReportDefinition.ReportObjects["tbSum"];
            tbSum.Text = strings.Price;

            return reports;
        }

        protected override void SetFunctionKey()
        {
            base.SetFunctionKey();
            EnableButton(BtnF2, "印刷 (F2)");
        }

        protected override void BtnF2_Click(object sender = null, EventArgs e = null)
        {
            crystalReportViewer1.PrintReport();
        }

        public class ShipmentStrings
        {
            public string CustomerName { get; set; } = "";
            public string ShipmentNo { get; set; } = "";
            public string DeadLine { get; set; } = "";
            private string price;
            public string Price
            {
                get
                {
                    return price;
                }
                set
                {
                    price = MyConvert.AddComma(value, true);
                }
            }
        }
    }
}
