﻿using Ih31Azuma.Database;
using Ih31Azuma.MyConstant;
using Ih31Azuma.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Ih31Azuma.MyConstant.MessageProvider;
using iTextSharp;
using static Ih31Azuma.OrdersBuyPurchaceAndQuotationForm;

namespace Ih31Azuma
{
    public partial class OrdersAcceptingForm : IFormDesign
    {
        /*
         * 編集か新規作成化を表す定数
         * 0なら新規作成　1なら編集
         */

        public int NowMode { private get; set; } = 0;

        DataSet dset;
        //SQL用文字列
        StringBuilder sb;

        //SQLに使うtableを指定する変数
        string table = "orderreceive";
        Value value;
        string customerNo;//顧客番号
        int getReceivable;//売掛金額

        public OrdersAcceptingForm()
        {
            InitializeComponent();

            //税の計算をするときに使う予定？
            //            Program.ConstantProvider.TaxArray
            //Console.WriteLine("モード{0}", ModeNow);

            EnableDataGridViewOperator();
        }

        /**
         *編集モードで呼び出し時の表示
         */
        string editReceiveNo;
        public OrdersAcceptingForm(Value value)
        {
            InitializeComponent();
            btAdd.Text = "更新";
            btRegistration.Enabled = false;//登録ボタンの無効化
            btRegistration.Visible = false;//登録ボタンの非表示

            this.value = value;
            editReceiveNo = value.OrderreceiveNo; //編集する受注番号
            string editCustomerNo = value.CustomerNo;
            string editCustomerName = value.CustomerName;
            tbCustomerName.Text = editCustomerName;
            //Console.WriteLine("モード{0}", ModeNow);

            StringBuilder sbCS = new StringBuilder();
            sbCS.Append("SELECT (customers_creditlimit -" +
                " COALESCE(SUM(customersledger_accountsreceivable), 0))" +
                " FROM customers" +
                " LEFT OUTER JOIN customersledger" +
                " ON customersledger_client_no = customers_no" +
                " AND customersledger_is_done = 0" +
                " WHERE customers_no = " + editCustomerNo +
                " GROUP BY customers_no");


            sb = new StringBuilder();
            sb.Append("SELECT orderreceive_serialnumber, orderreceive_carname," +
                " orderreceive_modelyear, orderreceive_remarks," +
                " orderreceive_deliverydate, orders_price" +
                " FROM " + table + " JOIN orders" +
                " ON orderreceive_no = orders_orderreceive_no" +
                " AND orderreceive_serialnumber = orders_orderreceive_serialnumber" +
                " WHERE orderreceive_no = " + editReceiveNo);

            DatabaseConnector conn = new DatabaseConnector();
            DataSet dsetCS = conn.GetDataSet(sbCS.ToString(), "customers");

            string receiveStr = dsetCS.Tables["customers"].Rows[0][0].ToString();
            lbReceivable.Text = lbReceivable.Text = MyConvert.AddComma(receiveStr, true);

            DataSet dset2 = conn.GetDataSet(sb.ToString(), table);
            dataGridView1.Columns.Clear();
            dataGridView1.DataSource = dset2.Tables[table];

            dataGridView1.Columns[0].Visible = true;
            dataGridView1.Columns[0].HeaderText = "No";
            dataGridView1.Columns[1].HeaderText = "車両名";
            dataGridView1.Columns[2].HeaderText = "年式";
            dataGridView1.Columns[3].HeaderText = "備考";
            dataGridView1.Columns[4].Visible = false;
            dataGridView1.Columns[5].HeaderText = "予算";

            dataGridView1.Columns[0].Width = 20;
            dataGridView1.Columns[1].Width = 250;
            dataGridView1.Columns[2].Width = 70;
            dataGridView1.Columns[3].Width = 320;
            dataGridView1.Columns[4].Width = 110;

            EnableDataGridViewOperator();
        }


        private void TbDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Check.IsExceptionKeys(e.KeyChar) || Check.IsNumeric(e.KeyChar.ToString()) || e.KeyChar == '/')
            {
                return;
            }

            e.Handled = true;
        }

        public void SetCustomerData(string customerNo, string name, int Receivable)
        {
            this.customerNo = customerNo;
            tbCustomerName.Text = name;
            getReceivable = Receivable;


            string receivable = getReceivable.ToString();
            //receivable = MyConvert.AddComma(receivable, true);

            lbReceivable.Text = MyConvert.AddComma(receivable, true);
        }

        private List<DialogMessage> errList;
        /**
         * 追加ボタンが押された時の処理
         * 編集モード時は更新ボタンとなる
         */
        private void btAdd_Click(object sender, EventArgs e)
        {
            errList = new List<DialogMessage>(); //以下同文
            //予算、車名もしくは備考の空白チェック
            if (tbCarName.Text == "" || tbNote.Text == "" && tbBudget.Text == "")
            {

                MessageBox.Show("予算と車名もしくは備考を入力して下さい");
                return;
            }
            else
            {
                tbMakerName.Select();
            }

            if (NowMode == 0)
            {
                errList = new List<DialogMessage>();
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dataGridView1);
                row.Cells[0].Value = tbMakerName.Text + " " + tbCarName.Text;
                row.Cells[1].Value = tbModelYear.Text;
                row.Cells[2].Value = tbBudget.Text;
                row.Cells[3].Value = tbNote.Text;
                dataGridView1.Rows.Add(row);
            }
            else
            {
                bool canUpdate = Update();
                //if (canUpdate)
                //{
                //    MessageBox.Show("更新完了");
                //}
                //else
                //{
                //    MessageBox.Show("失敗");
                //}
            }
            //ShowMessageBox(errList.ToArray()); //仕様かわる
        }

        private void btRegistration_Click(object sender, EventArgs e)
        {
            if (tbCustomerName.Text.Equals(""))
            {
                MessageBox.Show("顧客名が選択されていません。");
                return;
            }
            if (dataGridView1.Rows.Count == 0)
            {
                MessageBox.Show("データが入力されていません。");
                return;
            }


            List<DialogMessage> errList = new List<DialogMessage>(); //以下同文

            bool canInsert = Insert();
            Console.WriteLine(canInsert);
            if (canInsert)
            {
                DialogResult result = MessageBox.Show("登録完了", "完了通知", MessageBoxButtons.OK);
            }
            else
            {
                DialogResult result = MessageBox.Show("登録失敗", "完了通知", MessageBoxButtons.OK);
            }
            Close();
        }

        /**
         * インサートを実行するメソッド
         */
        private bool Insert()
        {
            List<DialogMessage> errList = new List<DialogMessage>(); //以下同文

            //現在の日付を取得
            DateTime dateTime = DateTime.Now;
            TimeSpan time = dateTime.TimeOfDay;
            DateTime date = dateTime.Date;

            DatabaseConnector connecterOrderreceive = new DatabaseConnector();//受注
            DatabaseConnector connecterOrders = new DatabaseConnector();//注文
            DatabaseConnector connecterCustomersledger = new DatabaseConnector();//得意先元帳

            int orderreceive_no = getOrderReceiveNo();//受注番号の取得        
            string order_no = orderreceive_no.ToString(); //注文番号の設定
            order_no = ConvetOrderReceiveNo(order_no);//ここで注文番号を設定する
            Console.WriteLine(order_no);
            StringBuilder sbOrders;//注文テーブルにインサートするSQL文字列
            StringBuilder sbCS;//得意先元帳テーブルにインサートするSQL文字列
            bool check = false;//INSERTが成功したかどうかの判定
            string carName;  //車名
            string modelYear;//年式
            int buget;       //予算
            int sumBuget = 0;//予算の合計を格納する変数
            string note;     //備考
            sb = new StringBuilder();
            sbOrders = new StringBuilder();
            for (int count = 0; count < dataGridView1.Rows.Count; count++)
            {
                //グリッドビューの１行分のデータを取得
                DataGridViewRow row = dataGridView1.Rows[count];
                carName = row.Cells[0].Value.ToString();//車名
                modelYear = row.Cells[1].Value.ToString();//年式
                buget = Convert.ToInt32(row.Cells[2].Value);//予算
                Console.WriteLine(buget);
                note = row.Cells[3].Value.ToString();//備考
                sumBuget += buget;


                //受注テーブルのインサート
                sb.Append("INSERT INTO  " + table +
                    "(orderreceive_no , orderreceive_serialnumber , orderreceive_carname  , orderreceive_modelyear ," +
                    "orderreceive_remarks , orderreceive_deliverydate , " +
                    "orderreceive_insert_date , orderreceive_insert_time, orderreceive_client_no) values" +
                    "(" + orderreceive_no + "," + (count + 1) + ",'" + carName + "','" + modelYear + "','" +
                     note + "','" + tbDeadline.Text + "','" + date + "','" + time + "'," +
                     customerNo + ")");

                //注文テーブルのインサート（注文番号,予算,登録日,登録時間,受注番号,連番）
                sbOrders.Append("INSERT INTO orders " +
                "(orders_no , orders_price , orders_insert_date , orders_insert_time ," +
                " orders_orderreceive_no , orders_orderreceive_serialnumber)" +
                " values " +
                "(" + order_no + "," + buget + ",'" + date + "','" + time + "'," +
                 orderreceive_no + "," + (count + 1) + ")");

                connecterOrderreceive.ExecuteNonQuery(sb.ToString());//受注テーブルのインサートSQL実行
                connecterOrders.ExecuteNonQuery(sbOrders.ToString());//注文テーブルのインサートSQL実行
                sb.Clear();
                sbOrders.Clear();
                check = true;
            }
            sbCS = new StringBuilder();//得意先元帳のインサート(受注番号,売掛金額,登録日,登録時間,顧客番号)
            sbCS.Append("INSERT INTO customersledger " +
             "(customersledger_orders_no , customersledger_accountsreceivable ," +
             " customersledger_insert_date, customersledger_insert_time ," +
             "customersledger_client_no)" +
             " values (" +
             orderreceive_no + "," + sumBuget + ",'" +
             date + "','" + time + "'," + customerNo + ")");
            connecterCustomersledger.ExecuteNonQuery(sbCS.ToString());//得意先元のインサート実行

            return check;
        }

        private bool Update()
        {
            DatabaseConnector connecterOrderreceive = new DatabaseConnector();//受注
            string serialNo = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            Console.WriteLine("連番{0}", serialNo);

            string carName = tbMakerName.Text + "　" + tbCarName.Text;
            sb = new StringBuilder();
            StringBuilder sbOrders = new StringBuilder();
            sb.Append("UPDATE " + table +
                " SET orderreceive_carname = '" + carName +
                "', orderreceive_modelyear = '" + tbModelYear.Text +
                "', orderreceive_remarks = '" + tbNote.Text +
                "', orderreceive_deliverydate = '" + tbDeadline.Text +
                "' WHERE  orderreceive_no = '" + editReceiveNo +
                "' AND orderreceive_serialnumber = '" + serialNo +
                "'");
            connecterOrderreceive.ExecuteNonQuery(sb.ToString());
            dataGridView1.Rows[rowIndex].Cells[1].Value = carName;
            dataGridView1.Rows[rowIndex].Cells[2].Value = tbModelYear.Text;
            dataGridView1.Rows[rowIndex].Cells[3].Value = tbNote.Text;

            string getBuget = tbBudget.Text;
            if (getBuget.Equals(""))
            {
                getBuget = "0";
            }
            dataGridView1.Rows[rowIndex].Cells[5].Value = getBuget;

            //注文テーブルのアップデート（予算）
            string order_no = ConvetOrderReceiveNo(editReceiveNo);//ここで注文番号を設定する
            DatabaseConnector connecterOrders = new DatabaseConnector();//注文
            sbOrders.Append("UPDATE orders " +
            "SET orders_price = " + getBuget +
            " WHERE orders_no = " + order_no +
            " AND orders_orderreceive_serialnumber = " + serialNo);
            connecterOrders.ExecuteNonQuery(sbOrders.ToString());


            int sumBuget = 0;
            for (int count = 0; count < dataGridView1.Rows.Count; count++)
            {
                DataGridViewRow row = dataGridView1.Rows[count];//１行分のデータ取得
                sumBuget += Convert.ToInt32(getBuget);//合計予算
            }
            //得意先元帳のアップデート(売掛金額)
            DatabaseConnector connecterCustomersledger = new DatabaseConnector();
            StringBuilder sbCS = new StringBuilder();
            sbCS.Append("UPDATE customersledger " +
             "SET customersledger_accountsreceivable = " + sumBuget +
             " WHERE customersledger_orders_no = " + editReceiveNo);
            connecterCustomersledger.ExecuteNonQuery(sbCS.ToString());//得意先元のアップデート実行


            bool check = false;
            return check;
        }

        private int getOrderReceiveNo()
        {
            sb = new StringBuilder();
            sb.Append("SELECT MAX(orderreceive_no) FROM " + table);
            string sql = sb.ToString();
            DatabaseConnector conn = new DatabaseConnector();
            dset = conn.GetDataSet(sql, table);
            conn.Disconnect();
            int getOrno = Convert.ToInt32(dset.Tables[table].Rows[0][0]) + 1;
            return getOrno;
        }
        /**
         * 受注番号を受け取り注文テーブルの注文番号に変更するメソッド
         */
        public static string ConvetOrderReceiveNo(string s)
        {

            if (!int.TryParse(s, out int no))
            {
                Error();
            }
            if (s.Length <= 5)
            {
                return (100000 + no).ToString();
            }
            else if (s.Length == 6 && s.Substring(0, 1) == "1")
            {
                return s;
            }
            Error();
            return null;
        }
        private static void Error()
        {
            throw new FormatException();
        }



        private void btSearch_Click(object sender, EventArgs e)
        {
            OrdersAcceptingSearchForm searchForm = new OrdersAcceptingSearchForm(this);
            searchForm.Show();
        }

        protected override void SetFunctionKey()
        {
            base.SetFunctionKey();
            EnableButton(BtnF1, "検索 (F1)");
            EnableButton(BtnF5, "追加 (F5)");
        }

        private void FoundKeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Check.IsExceptionKeys(e.KeyChar))
            {
                if (!Check.IsNumeric(e.KeyChar.ToString()))
                {
                    e.Handled = true;
                }
            }
        }

        protected override void BtnF1_Click(object sender, EventArgs e)
        {
            base.BtnF1_Click(sender, e);
            btSearch_Click(null, null);
        }

        protected override void FoundKeyDown(object sender, KeyEventArgs e)
        {
            base.FoundKeyDown(sender, e);
            if (e.KeyCode == Keys.Enter)
            {
                dataGridView1_CellDoubleClick(null, null);
            }
        }

        protected override void BtnF3_Click(object sender = null, EventArgs e = null)
        {
            MoveSelectRow(Keys.F3, dataGridView1);
        }
        protected override void BtnF4_Click(object sender = null, EventArgs e = null)
        {
            MoveSelectRow(Keys.F4, dataGridView1);
        }

        protected override void BtnF5_Click(object sender, EventArgs e)
        {
            base.BtnF5_Click(sender, e);
            MyMessageBox.Yes = "追加";
            if (NowMode == 1)
            {
                MyMessageBox.Yes = "更新";
            }

            MyMessageBox.No = "登録";
            MyMessageBox.Cancel = "Cancel";

            MyMessageBox.Register();
            DialogResult result = MessageBox.Show("処理を選択してください。", "処理選択", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                btAdd_Click(null, null);
            }
            if (result == DialogResult.No)
            {
                btRegistration_Click(null, null);
            }

            MyMessageBox.Unregister();
        }

        /**
         * 編集モード時セルが２回クリックされた時の処理
         */
        int rowIndex;//データグリッドビューの選択された行番号

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Rows.Count == 0)
            {
                return;
            }
            string names = dataGridView1.CurrentRow.Cells[1].Value.ToString();

            string[] splitNames = names.Split(null);//空白文字で分割
            tbMakerName.Text = splitNames[0];
            StringBuilder sbCarName = new StringBuilder();
            for (int count = 1; count < splitNames.Length; count++)
            {
                sbCarName.Append(splitNames[count]);
                if (count < splitNames.Length - 1)
                {
                    sbCarName.Append('　');
                }
            }
            tbCarName.Text = sbCarName.ToString();
            tbModelYear.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            tbNote.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            tbBudget.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            tbDeadline.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            rowIndex = dataGridView1.CurrentRow.Index;
            Console.WriteLine("行番号{0}", rowIndex);

        }

        private void OrdersAcceptingForm_Load(object sender, EventArgs e)
        {
            //            MessageBox.Show(NowMode.ToString());
        }
    }
}
