﻿using Ih31Azuma.Database;
using Ih31Azuma.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ih31Azuma
{
    public partial class OrdersAcceptingSearchForm : IFormDesign
    {
        private OrdersAcceptingForm ordersAcceptingForm;
        public OrdersAcceptingSearchForm(OrdersAcceptingForm ordersAcceptingForm)
        {
            this.ordersAcceptingForm = ordersAcceptingForm;
            InitializeComponent();
            EnableDataGridViewOperator();
            //dataGridView = dataGridView1;
        }

        //顧客名のセッター＆ゲッター
        public class Customer
        {
            public string customerNo { get; set; } = "";
            public string Name { get; set; } = "";
            public int Receivable { get; set; } = 0;
        }

        DataSet dset;

        protected override void SetFunctionKey()
        {
            base.SetFunctionKey();
            EnableButton(BtnF1, "検索 (F1)");
            EnableButton(BtnF5, "確定（F5)");
        }

        private void btSearch_Click(object sender, EventArgs e)
        {
            string table = "customers";
            StringBuilder sbSql = new StringBuilder();

            if (tbCustomerName.Text.Equals(""))
            {
                //sbSql.Append("SELECT customers_no, " +
                //    "customers_name," +
                //    "customers_pref," +
                //    "customers_address," +
                //    "customers_tel," +
                //    "customersledger_accountsreceivable " +
                //    "FROM " + table +
                //    " LEFT JOIN customersledger " +
                //    " ON customers_no = customersledger_client_no " +
                //    "WHERE customersledger_insert_time IN( " +
                //    "SELECT MAX(customersledger_insert_time) " +
                //    "FROM customersledger " +
                //    "GROUP BY customersledger_client_no)");

                sbSql.Append("SELECT customers_no, " +
                    "customers_name, customers_pref, customers_address,customers_tel, " +
                    "(customers_creditlimit - COALESCE(SUM(customersledger_accountsreceivable), 0)) " +
                    "as receivable FROM customers" +
                    " LEFT OUTER JOIN customersledger " +
                    "ON customersledger_client_no = customers_no " +
                    "AND customersledger_is_done = 0" +
                    " GROUP BY customers_no");
            }
            else
            {
                sbSql.Append("SELECT customers_no, " +
                   "customers_name, customers_pref, customers_address,customers_tel, " +
                   "(customers_creditlimit - COALESCE(SUM(customersledger_accountsreceivable), 0)) " +
                   "as receivable FROM customers" +
                   " LEFT OUTER JOIN customersledger " +
                   "ON customersledger_client_no = customers_no " +
                   "AND customersledger_is_done = 0" +
                   " WHERE customers_name LIKE '%" + tbCustomerName.Text + "%'" +
                   " GROUP BY customers_no");
            }

            string sql = sbSql.ToString();

            DatabaseConnector conn = new DatabaseConnector();
            dset = conn.GetDataSet(sql, table);
            conn.Disconnect();

            dataGridView1.DataSource = dset.Tables[table];

            dataGridView1.Columns[0].HeaderText = "顧客番号";
            dataGridView1.Columns[1].HeaderText = "顧客名";
            dataGridView1.Columns[2].HeaderText = "都道府県";
            dataGridView1.Columns[3].HeaderText = "住所";
            dataGridView1.Columns[4].HeaderText = "電話番号";
            dataGridView1.Columns[5].HeaderText = "売掛金額";

            dataGridView1.Columns[0].Width = 80;
            dataGridView1.Columns[1].Width = 120;
            dataGridView1.Columns[2].Width = 80;
            dataGridView1.Columns[3].Width = 191;
            dataGridView1.Columns[4].Width = 150;
            dataGridView1.Columns[5].Width = 150;
        }

        protected override void BtnF1_Click(object sender, EventArgs e)
        {
            base.BtnF1_Click(sender, e);
            btSearch_Click(null, null);
        }

        public void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if(dataGridView1.Rows.Count == 0)
            {
                return;
            }
            Customer customer = new Customer();
            //選択された列の顧客No
            customer.customerNo = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            //選択された列の顧客名
            customer.Name = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            //選択された列の売掛金額
            customer.Receivable = Convert.ToInt32(dataGridView1.CurrentRow.Cells[5].Value);
            Console.WriteLine(Convert.ToInt32(dataGridView1.CurrentRow.Cells[5].Value));

            ordersAcceptingForm.SetCustomerData(customer.customerNo, customer.Name, customer.Receivable);
            Close();
            Dispose();
        }

        protected override void FoundKeyDown(object sender, KeyEventArgs e)
        {
            base.FoundKeyDown(sender, e);
            if (e.KeyCode == Keys.Enter)
            {
                dataGridView1_CellDoubleClick(null, null);
            }
        }

        protected override void BtnF3_Click(object sender = null, EventArgs e = null)
        {
            MoveSelectRow(Keys.F3, dataGridView1);
        }
        protected override void BtnF4_Click(object sender = null, EventArgs e = null)
        {
            MoveSelectRow(Keys.F4, dataGridView1);
        }

        protected override void BtnF5_Click(object sender, EventArgs e)
        {
            base.BtnF5_Click(sender, e);
            dataGridView1_CellDoubleClick(null, null);
        }
    }
}