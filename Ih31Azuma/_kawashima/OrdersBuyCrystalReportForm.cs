﻿using CrystalDecisions.CrystalReports.Engine;
using iTextSharp.text;
using System;
using System.Data;
using System.Windows.Forms;
using static Ih31Azuma.OrdersBuyPurchaceAndQuotationForm;

namespace Ih31Azuma._kawashima
{
    public partial class OrdersBuyCrystalReportForm : IFormDesign
    {
        DataSet dataSet;
        Value value;

        ChangeDocumentDetail documentDetail;

        public class ChangeDocumentDetail
        {
            public String NoName { get; set; } = "";
            public String DateName { get; set; } = "";
            public String DocumentName { set; get; } = "";
            public String Message { get; set; } = "";
            public String PriceName { get; set; } = "";
            public String SubTotal { get; set; } = "";
            public String Consumption { get; set; } = "";
            public String Total { get; set; } = "";
        }

        public OrdersBuyCrystalReportForm(Value value, DataSet dataSet, ChangeDocumentDetail documentDetail)
        {
            InitializeComponent();
            this.value = value;
            this.dataSet = dataSet;
            this.documentDetail = documentDetail;
        }

        private void OrdersBuyCrystalReportViewer_Load(object sender, EventArgs e)
        {
            OrdersCrystalReport report = new OrdersCrystalReport();
            report.SetDataSource(dataSet);

            TextObject noName = (TextObject)report.ReportDefinition.ReportObjects["TxtNoName"];
            noName.Text = documentDetail.NoName;

            TextObject dateName = (TextObject)report.ReportDefinition.ReportObjects["TxtDateName"];
            dateName.Text = documentDetail.DateName;

            TextObject orederreceiveNo = (TextObject)report.ReportDefinition.ReportObjects["TxtOrderreceiveNo"];
            orederreceiveNo.Text = value.OrderreceiveNo;

            TextObject customerName = (TextObject)report.ReportDefinition.ReportObjects["TxtCustomerName"];
            customerName.Text = value.CustomerName + "　　御中";

            TextObject documentName = (TextObject)report.ReportDefinition.ReportObjects["TxtDocumentName"];
            documentName.Text = documentDetail.DocumentName;

            TextObject message = (TextObject)report.ReportDefinition.ReportObjects["TxtMessage"];
            message.Text = documentDetail.Message;

            TextObject priceName = (TextObject)report.ReportDefinition.ReportObjects["TxtPriceName"];
            priceName.Text = documentDetail.PriceName;

            TextObject subTotal = (TextObject)report.ReportDefinition.ReportObjects["TxtSubTotal"];
            subTotal.Text = documentDetail.SubTotal;

            TextObject consumption = (TextObject)report.ReportDefinition.ReportObjects["TxtConsumption"];
            consumption.Text = documentDetail.Consumption;

            TextObject total = (TextObject)report.ReportDefinition.ReportObjects["TxtTotal"];
            total.Text = documentDetail.Total;

            report.SetDataSource(dataSet);

            OrdersBuyCrystalReportViewer.ReportSource = report;
            OrdersBuyCrystalReportViewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
        }

        protected override void SetFunctionKey()
        {
            base.SetFunctionKey();
            EnableButton(BtnF2, "印刷 (F2)");
        }


        protected override void BtnF2_Click(object sender = null, EventArgs e = null)
        {
            base.BtnF2_Click(sender, e);
            OrdersBuyCrystalReportViewer.PrintReport();
        }
    }
}
