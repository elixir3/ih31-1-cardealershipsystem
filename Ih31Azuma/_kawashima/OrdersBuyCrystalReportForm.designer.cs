﻿namespace Ih31Azuma._kawashima
{
    partial class OrdersBuyCrystalReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OrdersBuyCrystalReportViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // OrdersBuyCrystalReportViewer
            // 
            this.OrdersBuyCrystalReportViewer.ActiveViewIndex = -1;
            this.OrdersBuyCrystalReportViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.OrdersBuyCrystalReportViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.OrdersBuyCrystalReportViewer.Location = new System.Drawing.Point(0, 0);
            this.OrdersBuyCrystalReportViewer.Name = "OrdersBuyCrystalReportViewer";
            this.OrdersBuyCrystalReportViewer.Size = new System.Drawing.Size(984, 480);
            this.OrdersBuyCrystalReportViewer.TabIndex = 0;
            this.OrdersBuyCrystalReportViewer.Load += new System.EventHandler(this.OrdersBuyCrystalReportViewer_Load);
            // 
            // OrdersBuyCrystalReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.OrdersBuyCrystalReportViewer);
            this.Name = "OrdersBuyCrystalReportForm";
            this.Text = "印刷画面";
            this.Controls.SetChildIndex(this.LbStatusBar, 0);
            this.Controls.SetChildIndex(this.BtnF1, 0);
            this.Controls.SetChildIndex(this.BtnF2, 0);
            this.Controls.SetChildIndex(this.BtnF3, 0);
            this.Controls.SetChildIndex(this.BtnF4, 0);
            this.Controls.SetChildIndex(this.BtnF5, 0);
            this.Controls.SetChildIndex(this.BtnF7, 0);
            this.Controls.SetChildIndex(this.BtnF6, 0);
            this.Controls.SetChildIndex(this.OrdersBuyCrystalReportViewer, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer OrdersBuyCrystalReportViewer;
    }
}