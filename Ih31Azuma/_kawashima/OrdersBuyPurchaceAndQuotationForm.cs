﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Ih31Azuma.Database;
using static Ih31Azuma._kawashima.OrdersBuyCrystalReportForm;
using Ih31Azuma._kawashima;
using Ih31Azuma.Util;
using static Ih31Azuma.MyConstant.MessageProvider;

//orderreceive_statusの　0は普通　1は見積書印刷済み　　4は出荷完了


namespace Ih31Azuma
{
    public partial class OrdersBuyPurchaceAndQuotationForm : IFormDesign
    {
        DataSet dataSet = null;
        public bool purchaseflg = false;

        private String Main { get; set; } = "";

        public class Value
        {
            public String OrderreceiveNo { get; set; } = "";
            public String CustomerName { get; set; } = "";
            public String CustomerNo { get; set; } = "";
            public String OrderreceiveDeliveryDate { get; set; } = "";
        }

        Value value;

        public OrdersBuyPurchaceAndQuotationForm(Value value)
        {
            InitializeComponent();
            this.value = value;
            LbSubtraction.Visible = false;
            LbSetSubtraction.Visible = false;
        }

        protected override void SetFunctionKey()
        {
            base.SetFunctionKey();
            EnableButton(BtnF2, "印刷 (F2)");
        }

        private void OrdersBuyPurchaceAndQuotationForm_Load(object sender, EventArgs e)
        {
            LblSetOrderNo.Text = value.OrderreceiveNo;
            LbSetCustomerName.Text = value.CustomerName;

            DateTime dateTime = DateTime.Now;
            string date = dateTime.ToString("yyyy年MM月dd日");
            LbSetOrderDate.Text = date;
            LbSetDeliveryDate.Text = value.OrderreceiveDeliveryDate;

            string table = "orders";
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT SUM(orders_price) as sum_price ");
            sql.Append("FROM orders ");
            sql.Append("WHERE orders_orderreceive_no = ");
            sql.Append(value.OrderreceiveNo);
            sql.Append(" GROUP BY orders_orderreceive_no");

            DatabaseConnector connector = new DatabaseConnector();
            dataSet = connector.GetDataSet(sql.ToString(), table);

            //小計
            LbSetSubTotal.Text = MyConvert.AddComma(dataSet.Tables[table].Rows[0][0].ToString(), true);

            string consumption = dataSet.Tables[table].Rows[0][0].ToString();
            int task = Program.ConstantProvider.GetTaxValue();

            //消費税
            LbSetConsumption.Text = MyConvert.AddComma((Convert.ToInt32(consumption) * task / 100).ToString(), true);

            //合計
            LbSetTotal.Text = MyConvert.AddComma(Program.ConstantProvider.CalcIncludeTax(Convert.ToInt32(consumption)).ToString(), true);

            table = "orders";
            sql = new StringBuilder();
            sql.Append("SELECT customers_creditlimit as subtraction, customers_no  ");
            sql.Append("FROM customers ");
            sql.Append("INNER JOIN orderreceive ON orderreceive_client_no = customers_no ");
            sql.Append("WHERE orderreceive_no = ");
            sql.Append(value.OrderreceiveNo);
            sql.Append(" GROUP BY orderreceive_no");
            connector = new DatabaseConnector();
            dataSet = connector.GetDataSet(sql.ToString(), table);

            //差引残高
         //   LbSetSubtraction.Text = MyConvert.AddComma((Convert.ToInt32(dataSet.Tables[table].Rows[0][0])).ToString(), true);
               // - Program.ConstantProvider.CalcIncludeTax(Convert.ToInt32(consumption))).ToString(), true);

            //顧客番号
            value.CustomerNo = dataSet.Tables[table].Rows[0][1].ToString();

            table = "orderreceive";
            sql = new StringBuilder();
            sql.Append("SELECT orderreceive_carname, orderreceive_modelyear,");
            sql.Append("orderreceive_remarks, orders_price");
            sql.Append(" FROM ");
            sql.Append(table);
            sql.Append(" INNER JOIN orders ON (orders_orderreceive_no =  orderreceive_no AND orders_orderreceive_serialnumber = orderreceive_serialnumber)");
            sql.Append(" INNER JOIN customers ON orderreceive_client_no = customers_no");
            sql.Append(" WHERE orderreceive_no = ");
            sql.Append(value.OrderreceiveNo);

            connector = new DatabaseConnector();
            dataSet = connector.GetDataSet(sql.ToString(), table);

            OrderGrid.DataSource = dataSet.Tables[table];
            OrderGrid.Rows[0].Selected = false;

            OrderGrid.Columns[0].HeaderText = "車名";
            OrderGrid.Columns[1].HeaderText = "年式";
            OrderGrid.Columns[2].HeaderText = "備考";
            OrderGrid.Columns[3].HeaderText = "金額";

            OrderGrid.Columns[0].Width = 263;
            OrderGrid.Columns[1].Width = 88;
            OrderGrid.Columns[2].Width = 333;
            OrderGrid.Columns[3].Width = 140;

            table = "orderreceive_data";
            sql = new StringBuilder();
            sql.Append("SELECT orderreceive_carname, orderreceive_modelyear,");
            sql.Append("orderreceive_remarks, orders_price, ");
            sql.Append("cast(FLOOR(orders_price * 1.08) AS char)as sum_price FROM ");
            sql.Append(table);
            sql.Append(" WHERE orderreceive_no = ");
            sql.Append(value.OrderreceiveNo);

            connector = new DatabaseConnector();
            dataSet = connector.GetDataSet(sql.ToString(), table);

            for (int index = 0; index < dataSet.Tables[table].Rows.Count; index++)
            {
                string price = dataSet.Tables[table].Rows[index][4].ToString();
                price = MyConvert.AddComma(price, true);
                dataSet.Tables[table].Rows[index][4] = price;
            }

            connector.Disconnect();

        }

        protected override void BtnF2_Click(object sender = null, EventArgs e = null)
        {
            base.BtnF2_Click(sender, e);
            MyMessageBox.Yes = "見積書";
            MyMessageBox.No = "注文書";
            MyMessageBox.Cancel = "Cancel";
            MyMessageBox.Register();
            DialogResult result = MessageBox.Show("どちらを印刷しますか。", "選択書類", MessageBoxButtons.YesNoCancel);
            MyMessageBox.Unregister();

            if (result == DialogResult.Yes)
            {
                BtPrintQuotation_Click(null, null);
            }
            else if (result == DialogResult.No)
            {
                BtPrintPurchace_Click(null, null);
            }
        }

        protected override void BtnF3_Click(object sender = null, EventArgs e = null)
        {
            base.BtnF3_Click(sender, e);
            MoveSelectRow(Keys.F3, OrderGrid);
        }

        protected override void BtnF4_Click(object sender = null, EventArgs e = null)
        {
            base.BtnF4_Click(sender, e);
            MoveSelectRow(Keys.F4, OrderGrid);
        }

        private void BtEdit_Click(object sender, EventArgs e)
        {
            OrdersAcceptingForm AcceptingForm = new OrdersAcceptingForm(value);

            AcceptingForm.NowMode = 1;
            AcceptingForm.Show();
        }

        /**
         * 注文（見積）番号を受け取り仕入れ台帳の仕入番号に変更するメソッド
         */
        public static string ConvetPurchaseNo(string s)
        {
            if (!int.TryParse(s, out int no))
            {
                Error();
            }
            if (s.Length <= 5)
            {
                return (300000 + no).ToString();
            }
            else if (s.Length == 6 && s.Substring(0, 1) == "3")
            {
                return s;
            }
            Error();
            return null;
        }

        private void InsertPurchase()
        {
            string purchase_no;
            string orderreceive_no;
            if (purchaseflg == true)
            {
                orderreceive_no = (Convert.ToInt32(value.OrderreceiveNo) % 100000).ToString();
                purchase_no = ConvetPurchaseNo(orderreceive_no);
            }
            else
            {
                orderreceive_no = value.OrderreceiveNo;
                purchase_no = ConvetPurchaseNo(orderreceive_no);
            }

            string purchase = "purchase";
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT purchase_no FROM " + purchase);
            sb.Append(" WHERE purchase_no = ");
            sb.Append(purchase_no);
            string sql = sb.ToString();
            DatabaseConnector conn = new DatabaseConnector();
            DataSet dataSet = conn.GetDataSet(sql, purchase);

            int rCnt = dataSet.Tables[purchase].Rows.Count;

            if (rCnt == 0)
            {
                for (int count = 1; count <= OrderGrid.Rows.Count; count++)
                {
                    //仕入れテーブルのインサート（仕入番号,受注番号,連番,登録日,登録時間）
                    string purchaseTable = "purchase";
                    StringBuilder pursb = new StringBuilder();
                    pursb.Append("INSERT INTO ");
                    pursb.Append(purchaseTable);
                    pursb.Append(" (purchase_no, purchase_orderreceive_no, purchase_orderreceive_serialnumber, purchase_insert_date, purchase_insert_time)");
                    pursb.Append(" VALUES ");
                    pursb.Append("(" + purchase_no + "," + orderreceive_no + "," + count + "," + "now()" + "," + "now()" + ")");

                    conn = new DatabaseConnector();
                    conn.ExecuteNonQuery(pursb.ToString());

                }
            }

            conn.Disconnect();
        }

        //見積書番号
        public static string ConvetOrderReceiveNo(string s)
        {
            if (!int.TryParse(s, out int no))
            {
                Error();
            }
            if (s.Length <= 5)
            {
                return (100000 + no).ToString();
            }
            else if (s.Length == 6 && s.Substring(0, 1) == "1")
            {
                return s;
            }
            Error();
            return null;
        }

        //注文書番号
        public static string ConvetReceiveNo(string s)
        {
            if (!int.TryParse(s, out int no))
            {
                Error();
            }
            if (s.Length <= 5)
            {
                return (200000 + no).ToString();
            }
            else if (s.Length == 6 && s.Substring(0, 1) == "2")
            {
                return s;
            }
            Error();
            return null;
        }
        private static void Error()
        {
            throw new FormatException();
        }

        private bool CheckCarName()
        {
            DatabaseConnector conn = new DatabaseConnector();

            String carTable = "orderreceive_data";
            StringBuilder CarSql = new StringBuilder();
            CarSql.Append("SELECT orderreceive_carname FROM ");
            CarSql.Append(carTable);
            CarSql.Append(" WHERE orderreceive_no = ");
            CarSql.Append(value.OrderreceiveNo);

            conn = new DatabaseConnector();
            DataSet statusSet = conn.GetDataSet(CarSql.ToString(), carTable);

            for (int index = 0; index < dataSet.Tables[carTable].Rows.Count; index++)
            {
                string carname = dataSet.Tables[carTable].Rows[index][0].ToString();

                if (carname.Equals(""))
                {
                    return false;

                }
            }
            return true;
        }

        private void BtPrintQuotation_Click(object sender, EventArgs e)
        {
            List<DialogMessage> blandlist = new List<DialogMessage>();

            if (LbSetDeliveryDate.Text.Equals(""))
            {
                blandlist.Add(DialogMessage.W010OrdersDeadlineIsEmpty);
            }

            if (!CheckCarName())
            {
                blandlist.Add(DialogMessage.W005OrdersCarIsEmpty);
            }

            ChangeDocumentDetail documentDetail = new ChangeDocumentDetail();
            documentDetail.NoName = "見積書番号";
            documentDetail.DateName = "見積日";
            documentDetail.DocumentName = "見積書";
            documentDetail.Message = "下記のとおり、御見積もり申し上げます。";
            documentDetail.PriceName = "ご請求金額";
            documentDetail.SubTotal = LbSetSubTotal.Text;
            documentDetail.Consumption = LbSetConsumption.Text;
            documentDetail.Total = LbSetTotal.Text;
            string orderreceive_no = value.OrderreceiveNo;

            try
            {
                value.OrderreceiveNo = ConvetOrderReceiveNo(orderreceive_no);
            }
            catch
            {
                DialogMessage[] message = blandlist.ToArray();
                ShowMessageBox(message);
                return;
            }

            purchaseflg = false;

            //見積書印刷済みのものにorderreceive_statusを1にする

            if (blandlist.Count == 0)
            {
                OrdersBuyCrystalReportForm reportForm = new OrdersBuyCrystalReportForm(value, dataSet, documentDetail);
                reportForm.Show();
            }
            else
            {
                DialogMessage[] message = blandlist.ToArray();
                ShowMessageBox(message);
            }


            DatabaseConnector conn = new DatabaseConnector();

            StringBuilder sql = new StringBuilder();
            sql.Append("Update orderreceive");
            sql.Append(" Set orderreceive_status= 1");
            sql.Append(" where orderreceive_no ='" + LblSetOrderNo.Text + "'");


            conn = new DatabaseConnector();
            conn.ExecuteNonQuery(sql.ToString());

            conn.Disconnect();
        }

        private void BtPrintPurchace_Click(object sender, EventArgs e)
        {
            List<DialogMessage> blandlist = new List<DialogMessage>();

            if (LbSetDeliveryDate.Text.Equals(""))
            {
                blandlist.Add(DialogMessage.W010OrdersDeadlineIsEmpty);
            }

            if (!CheckCarName())
            {
                blandlist.Add(DialogMessage.W005OrdersCarIsEmpty);
            }

            DatabaseConnector conn = new DatabaseConnector();

            int orderreceive_no = Convert.ToInt32(value.OrderreceiveNo) % 100000;
            string receive_no = orderreceive_no.ToString();

            string statusTable = "orderreceive";
            StringBuilder statusSql = new StringBuilder();
            statusSql.Append("SELECT orderreceive_status FROM orderreceive");
            statusSql.Append(" WHERE orderreceive_no = ");
            statusSql.Append(orderreceive_no);

            conn = new DatabaseConnector();
            DataSet statusSet = conn.GetDataSet(statusSql.ToString(), statusTable);

            DialogResult? result = null;
            if (statusSet.Tables[statusTable].Rows[0][0].ToString().Equals("0"))
            {
                //orderreceive_status = 0
                result = MessageBox.Show("見積書が印刷されていませんが、印刷しますか。", "確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

            }

            if (result == DialogResult.OK || !statusSet.Tables[statusTable].Rows[0][0].ToString().Equals("0"))
            {
                ChangeDocumentDetail documentDetail = new ChangeDocumentDetail();
                documentDetail.NoName = "注文書番号";
                documentDetail.DateName = "注文日";
                documentDetail.DocumentName = "注文書";
                documentDetail.Message = "下記のとおり、注文いたします。";
                documentDetail.PriceName = "発注金額";
                documentDetail.SubTotal = LbSetSubTotal.Text;
                documentDetail.Consumption = LbSetConsumption.Text;
                documentDetail.Total = LbSetTotal.Text;
                value.OrderreceiveNo = ConvetReceiveNo(receive_no);
                purchaseflg = true;

                if (blandlist.Count == 0)
                {
                    OrdersBuyCrystalReportForm reportForm = new OrdersBuyCrystalReportForm(value, dataSet, documentDetail);
                    reportForm.Show();
                }
                else
                {
                    DialogMessage[] message = blandlist.ToArray();
                    ShowMessageBox(message);
                }
            }

            conn.Disconnect();

            InsertPurchase();
        }
    }
}
