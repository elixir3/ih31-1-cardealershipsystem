﻿namespace Ih31Azuma
{
    partial class OrdersBuyPurchaceAndQuotationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtEdit = new System.Windows.Forms.Button();
            this.LbSetTotal = new System.Windows.Forms.Label();
            this.LbTotal = new System.Windows.Forms.Label();
            this.LbSetConsumption = new System.Windows.Forms.Label();
            this.LbConsumption = new System.Windows.Forms.Label();
            this.LbSetSubTotal = new System.Windows.Forms.Label();
            this.LbSubTotal = new System.Windows.Forms.Label();
            this.LbSetSubtraction = new System.Windows.Forms.Label();
            this.LbSubtraction = new System.Windows.Forms.Label();
            this.LbSetDeliveryDate = new System.Windows.Forms.Label();
            this.LbSetOrderDate = new System.Windows.Forms.Label();
            this.LbSetCustomerName = new System.Windows.Forms.Label();
            this.LblSetOrderNo = new System.Windows.Forms.Label();
            this.OrderGrid = new System.Windows.Forms.DataGridView();
            this.LbDeliveryDate = new System.Windows.Forms.Label();
            this.LbCustomerName = new System.Windows.Forms.Label();
            this.LbOrderDate = new System.Windows.Forms.Label();
            this.LbOrderNo = new System.Windows.Forms.Label();
            this.BtPrintPurchace = new System.Windows.Forms.Button();
            this.BtPrintQuotation = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.OrderGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnF2
            // 
            this.BtnF2.TabIndex = 4;
            // 
            // LbStatusBar
            // 
            this.LbStatusBar.Location = new System.Drawing.Point(12, 450);
            // 
            // BtEdit
            // 
            this.BtEdit.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtEdit.Location = new System.Drawing.Point(847, 304);
            this.BtEdit.Name = "BtEdit";
            this.BtEdit.Size = new System.Drawing.Size(100, 33);
            this.BtEdit.TabIndex = 1;
            this.BtEdit.Text = "編集";
            this.BtEdit.UseVisualStyleBackColor = true;
            this.BtEdit.Click += new System.EventHandler(this.BtEdit_Click);
            // 
            // LbSetTotal
            // 
            this.LbSetTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbSetTotal.Location = new System.Drawing.Point(526, 382);
            this.LbSetTotal.Name = "LbSetTotal";
            this.LbSetTotal.Size = new System.Drawing.Size(100, 16);
            this.LbSetTotal.TabIndex = 34;
            this.LbSetTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LbTotal
            // 
            this.LbTotal.AutoSize = true;
            this.LbTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbTotal.Location = new System.Drawing.Point(428, 382);
            this.LbTotal.Name = "LbTotal";
            this.LbTotal.Size = new System.Drawing.Size(72, 16);
            this.LbTotal.TabIndex = 33;
            this.LbTotal.Text = "合計見積";
            // 
            // LbSetConsumption
            // 
            this.LbSetConsumption.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbSetConsumption.Location = new System.Drawing.Point(526, 347);
            this.LbSetConsumption.Name = "LbSetConsumption";
            this.LbSetConsumption.Size = new System.Drawing.Size(100, 16);
            this.LbSetConsumption.TabIndex = 32;
            this.LbSetConsumption.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LbConsumption
            // 
            this.LbConsumption.AutoSize = true;
            this.LbConsumption.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbConsumption.Location = new System.Drawing.Point(428, 347);
            this.LbConsumption.Name = "LbConsumption";
            this.LbConsumption.Size = new System.Drawing.Size(56, 16);
            this.LbConsumption.TabIndex = 31;
            this.LbConsumption.Text = "消費税";
            // 
            // LbSetSubTotal
            // 
            this.LbSetSubTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbSetSubTotal.Location = new System.Drawing.Point(526, 313);
            this.LbSetSubTotal.Name = "LbSetSubTotal";
            this.LbSetSubTotal.Size = new System.Drawing.Size(100, 16);
            this.LbSetSubTotal.TabIndex = 30;
            this.LbSetSubTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LbSubTotal
            // 
            this.LbSubTotal.AutoSize = true;
            this.LbSubTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbSubTotal.Location = new System.Drawing.Point(428, 313);
            this.LbSubTotal.Name = "LbSubTotal";
            this.LbSubTotal.Size = new System.Drawing.Size(40, 16);
            this.LbSubTotal.TabIndex = 29;
            this.LbSubTotal.Text = "小計";
            // 
            // LbSetSubtraction
            // 
            this.LbSetSubtraction.AutoSize = true;
            this.LbSetSubtraction.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbSetSubtraction.Location = new System.Drawing.Point(143, 313);
            this.LbSetSubtraction.Name = "LbSetSubtraction";
            this.LbSetSubtraction.Size = new System.Drawing.Size(0, 16);
            this.LbSetSubtraction.TabIndex = 28;
            // 
            // LbSubtraction
            // 
            this.LbSubtraction.AutoSize = true;
            this.LbSubtraction.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbSubtraction.Location = new System.Drawing.Point(40, 313);
            this.LbSubtraction.Name = "LbSubtraction";
            this.LbSubtraction.Size = new System.Drawing.Size(72, 16);
            this.LbSubtraction.TabIndex = 27;
            this.LbSubtraction.Text = "差引残高";
            // 
            // LbSetDeliveryDate
            // 
            this.LbSetDeliveryDate.AutoSize = true;
            this.LbSetDeliveryDate.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbSetDeliveryDate.Location = new System.Drawing.Point(513, 59);
            this.LbSetDeliveryDate.Name = "LbSetDeliveryDate";
            this.LbSetDeliveryDate.Size = new System.Drawing.Size(0, 16);
            this.LbSetDeliveryDate.TabIndex = 26;
            // 
            // LbSetOrderDate
            // 
            this.LbSetOrderDate.AutoSize = true;
            this.LbSetOrderDate.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbSetOrderDate.Location = new System.Drawing.Point(513, 23);
            this.LbSetOrderDate.Name = "LbSetOrderDate";
            this.LbSetOrderDate.Size = new System.Drawing.Size(13, 16);
            this.LbSetOrderDate.TabIndex = 25;
            this.LbSetOrderDate.Text = " ";
            // 
            // LbSetCustomerName
            // 
            this.LbSetCustomerName.AutoSize = true;
            this.LbSetCustomerName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbSetCustomerName.Location = new System.Drawing.Point(143, 59);
            this.LbSetCustomerName.Name = "LbSetCustomerName";
            this.LbSetCustomerName.Size = new System.Drawing.Size(0, 16);
            this.LbSetCustomerName.TabIndex = 24;
            // 
            // LblSetOrderNo
            // 
            this.LblSetOrderNo.AutoSize = true;
            this.LblSetOrderNo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LblSetOrderNo.Location = new System.Drawing.Point(143, 23);
            this.LblSetOrderNo.Name = "LblSetOrderNo";
            this.LblSetOrderNo.Size = new System.Drawing.Size(0, 16);
            this.LblSetOrderNo.TabIndex = 23;
            // 
            // OrderGrid
            // 
            this.OrderGrid.AllowUserToAddRows = false;
            this.OrderGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OrderGrid.Enabled = false;
            this.OrderGrid.Location = new System.Drawing.Point(37, 102);
            this.OrderGrid.Name = "OrderGrid";
            this.OrderGrid.ReadOnly = true;
            this.OrderGrid.RowHeadersVisible = false;
            this.OrderGrid.RowTemplate.Height = 21;
            this.OrderGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.OrderGrid.Size = new System.Drawing.Size(827, 175);
            this.OrderGrid.TabIndex = 0;
            this.OrderGrid.TabStop = false;
            // 
            // LbDeliveryDate
            // 
            this.LbDeliveryDate.AutoSize = true;
            this.LbDeliveryDate.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbDeliveryDate.Location = new System.Drawing.Point(426, 59);
            this.LbDeliveryDate.Name = "LbDeliveryDate";
            this.LbDeliveryDate.Size = new System.Drawing.Size(40, 16);
            this.LbDeliveryDate.TabIndex = 21;
            this.LbDeliveryDate.Text = "納期";
            // 
            // LbCustomerName
            // 
            this.LbCustomerName.AutoSize = true;
            this.LbCustomerName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbCustomerName.Location = new System.Drawing.Point(40, 59);
            this.LbCustomerName.Name = "LbCustomerName";
            this.LbCustomerName.Size = new System.Drawing.Size(56, 16);
            this.LbCustomerName.TabIndex = 20;
            this.LbCustomerName.Text = "顧客名";
            // 
            // LbOrderDate
            // 
            this.LbOrderDate.AutoSize = true;
            this.LbOrderDate.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbOrderDate.Location = new System.Drawing.Point(426, 23);
            this.LbOrderDate.Name = "LbOrderDate";
            this.LbOrderDate.Size = new System.Drawing.Size(56, 16);
            this.LbOrderDate.TabIndex = 19;
            this.LbOrderDate.Text = "注文日";
            // 
            // LbOrderNo
            // 
            this.LbOrderNo.AutoSize = true;
            this.LbOrderNo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbOrderNo.Location = new System.Drawing.Point(40, 23);
            this.LbOrderNo.Name = "LbOrderNo";
            this.LbOrderNo.Size = new System.Drawing.Size(72, 16);
            this.LbOrderNo.TabIndex = 18;
            this.LbOrderNo.Text = "受注番号";
            // 
            // BtPrintPurchace
            // 
            this.BtPrintPurchace.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtPrintPurchace.Location = new System.Drawing.Point(807, 409);
            this.BtPrintPurchace.Name = "BtPrintPurchace";
            this.BtPrintPurchace.Size = new System.Drawing.Size(140, 33);
            this.BtPrintPurchace.TabIndex = 3;
            this.BtPrintPurchace.Text = "注文書印刷";
            this.BtPrintPurchace.UseVisualStyleBackColor = true;
            this.BtPrintPurchace.Click += new System.EventHandler(this.BtPrintPurchace_Click);
            // 
            // BtPrintQuotation
            // 
            this.BtPrintQuotation.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtPrintQuotation.Location = new System.Drawing.Point(807, 356);
            this.BtPrintQuotation.Name = "BtPrintQuotation";
            this.BtPrintQuotation.Size = new System.Drawing.Size(140, 33);
            this.BtPrintQuotation.TabIndex = 2;
            this.BtPrintQuotation.Text = "見積書印刷";
            this.BtPrintQuotation.UseVisualStyleBackColor = true;
            this.BtPrintQuotation.Click += new System.EventHandler(this.BtPrintQuotation_Click);
            // 
            // OrdersBuyPurchaceAndQuotationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.BtPrintQuotation);
            this.Controls.Add(this.BtPrintPurchace);
            this.Controls.Add(this.BtEdit);
            this.Controls.Add(this.LbSetTotal);
            this.Controls.Add(this.LbTotal);
            this.Controls.Add(this.LbSetConsumption);
            this.Controls.Add(this.LbConsumption);
            this.Controls.Add(this.LbSetSubTotal);
            this.Controls.Add(this.LbSubTotal);
            this.Controls.Add(this.LbSetSubtraction);
            this.Controls.Add(this.LbSubtraction);
            this.Controls.Add(this.LbSetDeliveryDate);
            this.Controls.Add(this.LbSetOrderDate);
            this.Controls.Add(this.LbSetCustomerName);
            this.Controls.Add(this.LblSetOrderNo);
            this.Controls.Add(this.OrderGrid);
            this.Controls.Add(this.LbDeliveryDate);
            this.Controls.Add(this.LbCustomerName);
            this.Controls.Add(this.LbOrderDate);
            this.Controls.Add(this.LbOrderNo);
            this.Name = "OrdersBuyPurchaceAndQuotationForm";
            this.Text = "買見積書・注文書作成画面";
            this.Load += new System.EventHandler(this.OrdersBuyPurchaceAndQuotationForm_Load);
            this.Controls.SetChildIndex(this.BtnF7, 0);
            this.Controls.SetChildIndex(this.LbStatusBar, 0);
            this.Controls.SetChildIndex(this.BtnF1, 0);
            this.Controls.SetChildIndex(this.BtnF2, 0);
            this.Controls.SetChildIndex(this.BtnF3, 0);
            this.Controls.SetChildIndex(this.BtnF4, 0);
            this.Controls.SetChildIndex(this.BtnF5, 0);
            this.Controls.SetChildIndex(this.BtnF6, 0);
            this.Controls.SetChildIndex(this.LbOrderNo, 0);
            this.Controls.SetChildIndex(this.LbOrderDate, 0);
            this.Controls.SetChildIndex(this.LbCustomerName, 0);
            this.Controls.SetChildIndex(this.LbDeliveryDate, 0);
            this.Controls.SetChildIndex(this.OrderGrid, 0);
            this.Controls.SetChildIndex(this.LblSetOrderNo, 0);
            this.Controls.SetChildIndex(this.LbSetCustomerName, 0);
            this.Controls.SetChildIndex(this.LbSetOrderDate, 0);
            this.Controls.SetChildIndex(this.LbSetDeliveryDate, 0);
            this.Controls.SetChildIndex(this.LbSubtraction, 0);
            this.Controls.SetChildIndex(this.LbSetSubtraction, 0);
            this.Controls.SetChildIndex(this.LbSubTotal, 0);
            this.Controls.SetChildIndex(this.LbSetSubTotal, 0);
            this.Controls.SetChildIndex(this.LbConsumption, 0);
            this.Controls.SetChildIndex(this.LbSetConsumption, 0);
            this.Controls.SetChildIndex(this.LbTotal, 0);
            this.Controls.SetChildIndex(this.LbSetTotal, 0);
            this.Controls.SetChildIndex(this.BtEdit, 0);
            this.Controls.SetChildIndex(this.BtPrintPurchace, 0);
            this.Controls.SetChildIndex(this.BtPrintQuotation, 0);
            ((System.ComponentModel.ISupportInitialize)(this.OrderGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Button BtEdit;
        protected System.Windows.Forms.Label LbSetTotal;
        protected System.Windows.Forms.Label LbTotal;
        protected System.Windows.Forms.Label LbSetConsumption;
        protected System.Windows.Forms.Label LbConsumption;
        protected System.Windows.Forms.Label LbSetSubTotal;
        protected System.Windows.Forms.Label LbSubTotal;
        protected System.Windows.Forms.Label LbSetSubtraction;
        protected System.Windows.Forms.Label LbSubtraction;
        protected System.Windows.Forms.Label LbSetDeliveryDate;
        protected System.Windows.Forms.Label LbSetOrderDate;
        protected System.Windows.Forms.Label LbSetCustomerName;
        protected System.Windows.Forms.Label LblSetOrderNo;
        protected System.Windows.Forms.DataGridView OrderGrid;
        protected System.Windows.Forms.Label LbDeliveryDate;
        protected System.Windows.Forms.Label LbCustomerName;
        protected System.Windows.Forms.Label LbOrderDate;
        protected System.Windows.Forms.Label LbOrderNo;
        protected System.Windows.Forms.Button BtPrintPurchace;
        protected System.Windows.Forms.Button BtPrintQuotation;
    }
}