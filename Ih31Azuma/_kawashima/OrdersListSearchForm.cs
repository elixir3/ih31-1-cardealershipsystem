﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Ih31Azuma.Database;
using Ih31Azuma.Util;
using static Ih31Azuma.MyConstant.MessageProvider;
using static Ih31Azuma.OrdersBuyPurchaceAndQuotationForm;


namespace Ih31Azuma
{
    public partial class OrdersListSearchForm : IFormDesign
    {
        DataSet dataSet = null;

        public OrdersListSearchForm()
        {
            InitializeComponent();
            EnableDataGridViewOperator();
        }

        protected override void SetFunctionKey()
        {
            base.SetFunctionKey();
            EnableButton(BtnF1, "検索 (F1)");
            EnableButton(BtnF5, "確定（F5)");
        }

        private void OrdersList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BtnF5_Click();
        }

        protected override void BtnF1_Click(object sender = null, EventArgs e = null)
        {
            base.BtnF1_Click(sender, e);

            string table = "orderreceive";
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT cast(orderreceive_no as char), customers_name, orderreceive_deliverydate");
            sql.Append(" FROM ");
            sql.Append(table);
            sql.Append(" LEFT JOIN customers ON orderreceive_client_no = customers_no");
            sql.Append(" WHERE orderreceive_serialnumber = 1");
            
            //受注番号が未入力の場合
            if (!TxOrderNumber.Text.Equals(""))
            {
                sql.Append(" AND orderreceive_no = ");
                sql.Append(TxOrderNumber.Text);
            }
            else if (!TxCustomerName.Text.Equals(""))
            {
                sql.Append(" AND customers_name LIKE '%");
                sql.Append(TxCustomerName.Text);
                sql.Append("%'");
            }
          
            DatabaseConnector connector = new DatabaseConnector();
            dataSet = connector.GetDataSet(sql.ToString(), table);


            int rCnt = dataSet.Tables[table].Rows.Count;

            List<DialogMessage> errorlist = new List<DialogMessage>();

            if (TxOrderNumber.Text != "" && rCnt == 0)
            {
                errorlist.Add(DialogMessage.W021SearchNotExsitOrdersNo);
            }

            if (TxCustomerName.Text != "" & rCnt == 0)
            {
                errorlist.Add(DialogMessage.W019SearchNotExsitName);
            }

            OrdersList.DataSource = dataSet.Tables[table];

            int width = OrdersList.Width;
            OrdersList.Columns[0].HeaderText = "受注番号";
            OrdersList.Columns[1].HeaderText = "顧客名";
            OrdersList.Columns[2].HeaderText = "納期";

            OrdersList.Columns[0].Width = 100;
            OrdersList.Columns[1].Width = width - 250 - 3;
            OrdersList.Columns[2].Width = 150;

            DialogMessage[] message = errorlist.ToArray();

            if (errorlist.Count != 0)
            {
                ShowMessageBox(message);
            }

            connector.Disconnect();
        }

        protected override void BtnF3_Click(object sender = null, EventArgs e = null)
        {
            base.BtnF3_Click(sender, e);
            MoveSelectRow(Keys.F3, OrdersList);
        }

        protected override void BtnF4_Click(object sender = null, EventArgs e = null)
        {
            base.BtnF4_Click(sender, e);
            MoveSelectRow(Keys.F4, OrdersList);
        }

        protected override void BtnF5_Click(object sender = null, EventArgs e = null)
        {

            if (OrdersList.Rows.Count == 0)
            {
                return;
            }

            Value value = new Value();
            value.OrderreceiveNo = OrdersList.CurrentRow.Cells["cast(orderreceive_no as char)"].Value.ToString();
            value.CustomerName = OrdersList.CurrentRow.Cells["customers_name"].Value.ToString();
            value.OrderreceiveDeliveryDate = OrdersList.CurrentRow.Cells["orderreceive_deliverydate"].Value.ToString();

            OrdersBuyPurchaceAndQuotationForm printForm = new OrdersBuyPurchaceAndQuotationForm(value);
            printForm.Show();
        }

        protected override void FoundKeyDown(object sender, KeyEventArgs e)
        {
            base.FoundKeyDown(sender, e);
            if (e.KeyCode == Keys.Enter)
            {
                OrdersList_CellDoubleClick(null, null);
            }
        }

        private void FoundKeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Check.IsExceptionKeys(e.KeyChar))
            {
                if (!Check.IsNumeric(e.KeyChar.ToString()))
                {
                    e.Handled = true;
                }
            }
        }
    }
}
