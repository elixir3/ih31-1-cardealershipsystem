﻿namespace Ih31Azuma
{
    partial class OrdersListSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OrdersList = new System.Windows.Forms.DataGridView();
            this.LbCustomerName = new System.Windows.Forms.Label();
            this.LbOrderNo = new System.Windows.Forms.Label();
            this.TxOrderNumber = new System.Windows.Forms.TextBox();
            this.TxCustomerName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.OrdersList)).BeginInit();
            this.SuspendLayout();
            // 
            // OrdersList
            // 
            this.OrdersList.AllowUserToAddRows = false;
            this.OrdersList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OrdersList.Location = new System.Drawing.Point(26, 129);
            this.OrdersList.Name = "OrdersList";
            this.OrdersList.ReadOnly = true;
            this.OrdersList.RowHeadersVisible = false;
            this.OrdersList.RowTemplate.Height = 21;
            this.OrdersList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.OrdersList.Size = new System.Drawing.Size(453, 311);
            this.OrdersList.TabIndex = 3;
            this.OrdersList.TabStop = false;
            this.OrdersList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OrdersList_CellDoubleClick);
            // 
            // LbCustomerName
            // 
            this.LbCustomerName.AutoSize = true;
            this.LbCustomerName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbCustomerName.Location = new System.Drawing.Point(64, 75);
            this.LbCustomerName.Name = "LbCustomerName";
            this.LbCustomerName.Size = new System.Drawing.Size(56, 16);
            this.LbCustomerName.TabIndex = 38;
            this.LbCustomerName.Text = "顧客名";
            // 
            // LbOrderNo
            // 
            this.LbOrderNo.AutoSize = true;
            this.LbOrderNo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LbOrderNo.Location = new System.Drawing.Point(64, 32);
            this.LbOrderNo.Name = "LbOrderNo";
            this.LbOrderNo.Size = new System.Drawing.Size(72, 16);
            this.LbOrderNo.TabIndex = 39;
            this.LbOrderNo.Text = "受注番号";
            // 
            // TxOrderNumber
            // 
            this.TxOrderNumber.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.TxOrderNumber.Location = new System.Drawing.Point(160, 29);
            this.TxOrderNumber.MaxLength = 5;
            this.TxOrderNumber.Name = "TxOrderNumber";
            this.TxOrderNumber.Size = new System.Drawing.Size(100, 23);
            this.TxOrderNumber.TabIndex = 1;
            this.TxOrderNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // TxCustomerName
            // 
            this.TxCustomerName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.TxCustomerName.Location = new System.Drawing.Point(160, 72);
            this.TxCustomerName.Name = "TxCustomerName";
            this.TxCustomerName.Size = new System.Drawing.Size(233, 23);
            this.TxCustomerName.TabIndex = 2;
            // 
            // OrdersListSearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.TxCustomerName);
            this.Controls.Add(this.TxOrderNumber);
            this.Controls.Add(this.LbOrderNo);
            this.Controls.Add(this.LbCustomerName);
            this.Controls.Add(this.OrdersList);
            this.Name = "OrdersListSearchForm";
            this.Text = "受注一覧検索画面";
            this.Controls.SetChildIndex(this.BtnF7, 0);
            this.Controls.SetChildIndex(this.OrdersList, 0);
            this.Controls.SetChildIndex(this.LbCustomerName, 0);
            this.Controls.SetChildIndex(this.LbStatusBar, 0);
            this.Controls.SetChildIndex(this.BtnF1, 0);
            this.Controls.SetChildIndex(this.BtnF2, 0);
            this.Controls.SetChildIndex(this.BtnF3, 0);
            this.Controls.SetChildIndex(this.BtnF4, 0);
            this.Controls.SetChildIndex(this.BtnF5, 0);
            this.Controls.SetChildIndex(this.BtnF6, 0);
            this.Controls.SetChildIndex(this.LbOrderNo, 0);
            this.Controls.SetChildIndex(this.TxOrderNumber, 0);
            this.Controls.SetChildIndex(this.TxCustomerName, 0);
            ((System.ComponentModel.ISupportInitialize)(this.OrdersList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView OrdersList;
        protected System.Windows.Forms.Label LbCustomerName;
        protected System.Windows.Forms.Label LbOrderNo;
        private System.Windows.Forms.TextBox TxOrderNumber;
        private System.Windows.Forms.TextBox TxCustomerName;
    }
}