﻿using Ih31Azuma.Database;
using Ih31Azuma.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Ih31Azuma.MyConstant.MessageProvider;

namespace Ih31Azuma
{
    public partial class PurchaseForm : IFormDesign
    {
        private string no = "";

        public PurchaseForm()
        {
            InitializeComponent();
            EnableDataGridViewOperator();
        }

        protected override void FoundKeyDown(object sender, KeyEventArgs e)
        {
            base.FoundKeyDown(sender, e);
            if (e.KeyCode == Keys.Enter)
            {
                DataGridView1_CellDoubleClick(null, null);
            }
        }

        protected override void SetFunctionKey()
        {
            base.SetFunctionKey();
            EnableButton(BtnF1, "検索 (F1)");
            EnableButton(BtnF5, "変更 (F5)");
            //EnableButton(BtnF6, "クリア (F6)");
        }

        protected override void BtnF3_Click(object sender = null, EventArgs e = null)
        {
            MoveSelectRow(Keys.F3, dataGridView1);
        }

        protected override void BtnF4_Click(object sender = null, EventArgs e = null)
        {
            MoveSelectRow(Keys.F4, dataGridView1);
        }

        protected override void BtnF6_Click(object sender = null, EventArgs e = null)
        {
            
        }

        protected override void BtnF5_Click(object sender = null, EventArgs e = null)
        {
            int rows = dataGridView1.Rows.Count;
            if (rows == 0)
            {
                return;
            }

            DatabaseConnector conn = new DatabaseConnector();
            StringBuilder sql = new StringBuilder();

            sql.Append("UPDATE `purchase` SET  ");
            sql.Append(" purchase_date = \"" + tbDate.Text + "\", ");
            sql.Append(" purchase_price = " + MyConvert.AddComma(tbPrice.Text, false) + ", ");
            sql.Append(" purchase_expenses = " + MyConvert.AddComma(tbExpenses.Text, false) + ", ");
            sql.Append(" purchase_car_number = \"" + tbCarNo.Text + "\" ");
            sql.Append(" WHERE purchase_no = " + no);
            sql.Append(" AND purchase_orderreceive_serialnumber = " + lbNo.Text);

            conn.ExecuteNonQuery(sql.ToString());
            GetData(conn);

            string table = "shipping";
            string orderNo = (Convert.ToInt32(no) % 300000).ToString();
            string shipmentNo = MyConvert.ConvetShipmentNo(orderNo);
            sql.Clear();

            sql.Append("SELECT 1 FROM " + table);
            sql.Append(" WHERE shipping_no = " + shipmentNo);
            DataSet dataSet = conn.GetDataSet(sql.ToString(), table);
            if (dataSet.Tables[table].Rows.Count == 0)
            {
                if (CheckComplete())
                {
                    InsertShipment(conn, shipmentNo, orderNo);
                }
            }

            int calc = 0;
            for (int count = 0; count < rows; count++)
            {
                calc += Convert.ToInt32(dataGridView1.Rows[count].Cells[5].Value);
                calc += Convert.ToInt32(dataGridView1.Rows[count].Cells[6].Value);
            }

            sql.Clear();
            sql.Append("Update customersledger Set customersledger_accountsreceivable = " + calc);
            sql.Append(" Where customersledger_orders_no = " + Convert.ToUInt32(no) % 300000);
            conn.ExecuteNonQuery(sql.ToString());

            conn.Disconnect();

            int index = Convert.ToInt32(lbNo.Text) - 1;
            if (index < 0)
            {
                index = 0;
            }
                dataGridView1.Rows[0].Selected = false;
            dataGridView1.Rows[index].Selected = true;
            dataGridView1.CurrentCell = dataGridView1[0, index];


        }

        private bool CheckComplete()
        {
            DataGridViewRow row;
            for(int index = 0; index < dataGridView1.Rows.Count; index++)
            {
                row = dataGridView1.Rows[index];
                string s =row.Cells[0].Value.ToString();
                string[] ss = s.Split(' ');

                for (int count = 0; count < row.Cells.Count; count++)
                {
                    if (Check.IsEmpty(row.Cells[count].Value.ToString()))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private void InsertShipment(DatabaseConnector conn, string shipmentNo, string orderNo)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("INSERT INTO `shipping` (`shipping_no`, `shipping_orders_no`) VALUE( ");
            sql.Append( shipmentNo+ ",");
            sql.Append( orderNo + ")");
            conn.ExecuteNonQuery(sql.ToString());
        }

        protected override void BtnF1_Click(object sender = null, EventArgs e = null)
        {
            if (Check.IsEmpty(tbNo.Text))
            {
                DialogMessage.W501PurchaseNotExsitAcceptingNo.ShowMessageBox();
                return;
            }

            lbNo.Text = "";
            lbCarName.Text = "";
            lbModelYear.Text = "";
            tbCarNo.Text = "";
            tbDate.Text = "";
            tbPrice.Text = "";
            tbExpenses.Text = "";

            DatabaseConnector conn = new DatabaseConnector();
            GetData(conn);
            conn.Disconnect();

            if (dataGridView1.Rows.Count != 0)
            {
                lbNo.Text = dataGridView1.Rows[0].Cells[0].Value.ToString();
                tbDate.Text = DateTime.Today.ToString("yyyy/MM/dd");
                lbCarName.Text = dataGridView1.Rows[0].Cells[1].Value.ToString();
                lbModelYear.Text = dataGridView1.Rows[0].Cells[2].Value.ToString();
                tbCarNo.Text = dataGridView1.Rows[0].Cells[3].Value.ToString();
                tbDate.Text = dataGridView1.Rows[0].Cells[4].Value.ToString();
                tbPrice.Text = dataGridView1.Rows[0].Cells[5].Value.ToString();
                tbExpenses.Text = dataGridView1.Rows[0].Cells[6].Value.ToString();

                SetDataGridViewProperty();

                dataGridView1.Rows[0].Selected = true;
                dataGridView1.CurrentCell = dataGridView1[0, 0];
            }
            else
            {
                DialogMessage.W501PurchaseNotExsitAcceptingNo.ShowMessageBox();
            }

        }

        private void GetData(DatabaseConnector conn)
        {
            string table = "purchase_list";
            no = MyConvert.ConvetPurchaseNo(tbNo.Text);
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT purchase_orderreceive_serialnumber, orderreceive_carname, ");
            sql.Append("orderreceive_modelyear, purchase_car_number, purchase_date, purchase_price, purchase_expenses ");
            sql.Append("FROM purchase INNER JOIN orderreceive ON orderreceive_no = purchase_orderreceive_no ");
            sql.Append("AND orderreceive_serialnumber = purchase_orderreceive_serialnumber WHERE ");
            sql.Append("purchase_no = " + no);

            DataSet dataSet = conn.GetDataSet(sql.ToString(), table);

            dataGridView1.Columns.Clear();
            dataGridView1.DataSource = dataSet.Tables[table];

            SetDataGridViewProperty();
        }

        private void SetDataGridViewProperty()
        {
            string[] headerText = new string[] { "No", "車名", "年式", "形式車台番号", "仕入日", "仕入金額", "経費" };

            for (int count = 0; count < headerText.Length; count++)
            {
                dataGridView1.Columns[count].HeaderText = headerText[count];
            }


            int width = dataGridView1.Size.Width;
            int scrollBar = 17;
            if (dataGridView1.Rows.Count > 25)
            {
                width = width - scrollBar;
            }

            dataGridView1.Columns[0].Width = 30;
            dataGridView1.Columns[2].Width = 70;
            dataGridView1.Columns[3].Width = 120;
            dataGridView1.Columns[4].Width = 120;
            dataGridView1.Columns[5].Width = 120;
            dataGridView1.Columns[6].Width = 120;

            dataGridView1.Columns[1].Width = width - (120 * 4 + 50 * 2 + 3);

            var textCenter = DataGridViewContentAlignment.MiddleCenter;
            var textRight = DataGridViewContentAlignment.MiddleRight;
            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = textCenter;
            dataGridView1.Columns[0].DefaultCellStyle.Alignment = textRight;
            dataGridView1.Columns[5].DefaultCellStyle.Alignment = textRight;
            dataGridView1.Columns[6].DefaultCellStyle.Alignment = textRight;
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            BtnF1_Click();
        }

        private void FoundKeyPress(object sender, KeyPressEventArgs e)
        {
            if (Check.IsExceptionKeys(e.KeyChar) || Check.IsNumeric(e.KeyChar.ToString()))
            {
                return;
            }

            e.Handled = true;
        }

        private void TextBox_Enter(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (tb == null)
            {
                return;
            }

            try
            {
                tb.Text = MyConvert.AddComma(tb.Text, false);
            }
            catch
            {
                return;
            }
        }

        private void TextBox_Leave(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (tb == null)
            {
                return;
            }

            try
            {
                tb.Text = MyConvert.AddComma(tb.Text, true);
            }
            catch
            {
                return;
            }
        }

        private void DataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Rows.Count == 0)
            {
                return;
            }
            lbNo.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            lbCarName.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            lbModelYear.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            tbCarNo.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            tbDate.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            tbPrice.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            tbExpenses.Text = dataGridView1.CurrentRow.Cells[6].Value.ToString();
        }

        private void TbDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Check.IsExceptionKeys(e.KeyChar) || Check.IsNumeric(e.KeyChar.ToString()) || e.KeyChar == '/')
            {
                return;
            }

            e.Handled = true;
        }
    }
}
