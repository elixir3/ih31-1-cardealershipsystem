﻿using System;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using static Ih31Azuma.CollectForm;
using CrystalDecisions.Shared;
using System.IO;

namespace Ih31Azuma._sadamitsu
{
    public partial class ChargeCrystalReportPrint : IFormDesign
    {
        DataSet dataSet;
        ChangeDocumentDetail documentDetail;

        public class ChangeDocumentDetail
        {
            public String CustomerName { get; set; } = "";
            public String CollectNum { get; set; } = "";
            public String CollectBillingDate { get; set; } = "";    //請求日
            public String Total { get; set; } = "";     //合計金額
            public String TaxTotal { get; set; } = "";  //仕入金額（税込）＋手数料＋経費
        }

        protected override void SetFunctionKey()
        {
            base.SetFunctionKey();
            EnableButton(BtnF2, "印刷 (F2)");
        }

        public ChargeCrystalReportPrint(DataSet dataSet, ChangeDocumentDetail documentDetail)
        {
            InitializeComponent();
            this.dataSet = dataSet;
            this.documentDetail = documentDetail;
        }

        public ChargeCrystalReportPrint(DataSet dataSet, ChangeDocumentDetail documentDetail, int line)
        {
            InitializeComponent();
            this.dataSet = dataSet;
            this.documentDetail = documentDetail;

            CreateFolder();
            ExportPdf(line);
        }

        private void CreateFolder()
        {
            string path = "charge";
            if (!Directory.Exists(@path))
            {
                Directory.CreateDirectory(@path);
            }
        }

        private void ExportPdf(int line)
        {
            MyChargeReport1.SetDataSource(dataSet);

            TextObject CollectNum = (TextObject)MyChargeReport1.ReportDefinition.ReportObjects["TxtCollectNum"];
            CollectNum.Text = documentDetail.CollectNum;

            TextObject CustomerName = (TextObject)MyChargeReport1.ReportDefinition.ReportObjects["TxtCustomerName"];
            CustomerName.Text = documentDetail.CustomerName;

            TextObject CollectBillingDate = (TextObject)MyChargeReport1.ReportDefinition.ReportObjects["TxtCollectBillingDate"];
            CollectBillingDate.Text = documentDetail.CollectBillingDate;

            TextObject taxTotal = (TextObject)MyChargeReport1.ReportDefinition.ReportObjects["TxtTaxTotal"];
            taxTotal.Text = documentDetail.TaxTotal;

            MyChargeReport1.SetDataSource(dataSet);

            DiskFileDestinationOptions disk = new DiskFileDestinationOptions();
            disk.DiskFileName = "charge/" + line + ".pdf";
            ExportOptions opt = new ExportOptions();
            opt = MyChargeReport1.ExportOptions;
            opt.ExportDestinationType = ExportDestinationType.DiskFile;
            opt.ExportFormatType = ExportFormatType.PortableDocFormat;
            opt.ExportFormatOptions = new PdfRtfWordFormatOptions();
            opt.ExportDestinationOptions = disk;

            MyChargeReport1.Export();
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

            //クリスタルレポートを取ってくる
            var report = new MyChargeReport();

            //データ格納
            report.SetDataSource(dataSet);

            TextObject CollectNum = (TextObject)report.ReportDefinition.ReportObjects["TxtCollectNum"];
            CollectNum.Text = documentDetail.CollectNum;

            TextObject CustomerName = (TextObject)report.ReportDefinition.ReportObjects["TxtCustomerName"];
            CustomerName.Text = documentDetail.CustomerName;

            TextObject CollectBillingDate = (TextObject)report.ReportDefinition.ReportObjects["TxtCollectBillingDate"];
            CollectBillingDate.Text = documentDetail.CollectBillingDate;

            //TextObject total = (TextObject)report.ReportDefinition.ReportObjects["TxtTotal"];
            //total.Text = documentDetail.Total;

            TextObject taxTotal = (TextObject)report.ReportDefinition.ReportObjects["TxtTaxTotal"];
            taxTotal.Text = documentDetail.TaxTotal;

            report.SetDataSource(dataSet);

            //クリスタルレポートデータ表示
            crystalReportViewer1.ReportSource = report;

            //クリスタルレポート表示の際の横の空白をなくす
            crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
        }

        protected override void BtnF2_Click(object sender, EventArgs e)
        {
            base.BtnF2_Click(sender, e);
            crystalReportViewer1.PrintReport();
        }
    }
}
