﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Ih31Azuma.Database;
using Ih31Azuma.Util;
using iTextSharp.text;
using static Ih31Azuma.MyConstant.MessageProvider;

namespace Ih31Azuma
{
    public partial class ChargeForm : IFormDesign
    {
        protected Button submit;
        private DataSet dataSet;

        //入金完了済みのものは表に出さない sql.Append(" AND  collect_is_collected = 0 ");で対処
        //消込が全件にかかっている  対処
        //請求時に請求日時をcollect_billing_dateを挿入する（印刷した日） おけ
        //chargeForm 請求日時両方必要？
        //Crystalレポートの合計金額、税込み合計金額にカンマを三桁ずつ入れる　　おけ

        public ChargeForm()
        {
            InitializeComponent();
            submit = this.btnComp;
            EnableDataGridViewOperator();
        }
        protected override void SetFunctionKey()
        {
            base.SetFunctionKey();
            EnableButton(BtnF1, "検索 (F1)");
            EnableButton(BtnF5, "確定 (F5)");
        }

        private void FoundKeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Check.IsExceptionKeys(e.KeyChar))
            {
                if (!Check.IsNumeric(e.KeyChar.ToString()))
                {
                    e.Handled = true;
                }
            }
        }

        protected override void BtnF1_Click(object sender, EventArgs e)
        {
            base.BtnF1_Click(sender, e);
            btnSearch_Click(null, null);
        }

        protected override void BtnF5_Click(object sender = null, EventArgs e = null)
        {
            base.BtnF5_Click(sender, e);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string table = "collect";
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT collect_is_collected, collect_order_no, collect_billing_date, collect_closing_date, collect_price FROM collect");
            sql.Append(" INNER JOIN orders");
            sql.Append(" ON collect_order_no = orders_orderreceive_no");
            sql.Append(" INNER JOIN orderreceive");
            sql.Append(" ON(orderreceive_no = orders_orderreceive_no");
            sql.Append(" AND orderreceive_serialnumber = orders_orderreceive_serialnumber)");
            sql.Append(" WHERE orderreceive_serialnumber = 1 ");
            sql.Append(" AND  collect_is_collected = 0 ");
            sql.Append(" AND collect_billing_date IS NOT NULL");

            if (!txtYear.Text.Equals("") && !txtMonth.Text.Equals("") && !txtCollectNum.Text.Equals(""))
            {
                sql.Append(" AND collect_closing_date LIKE '%" + txtYear.Text + "" + "/" + "" + txtMonth.Text + "%'");
                sql.Append(" AND collect_order_no = '" + txtCollectNum.Text + "'");
            }
            else if (!txtYear.Text.Equals("") && !txtMonth.Text.Equals(""))
            {
                sql.Append(" AND  collect_closing_date LIKE '%" + txtYear.Text + "" + "/" + "" + txtMonth.Text + "%'");
            }
            else if (!txtCollectNum.Text.Equals(""))
            {
                sql.Append(" AND collect_order_no = '" + txtCollectNum.Text + "'");
            }

            sql.Append(" order by collect_closing_date ");
            DatabaseConnector dbco = new DatabaseConnector();
            dataSet = dbco.GetDataSet(sql.ToString(), table);
            dbco.Disconnect();
            SetdataGridView(table);
        }

        private void btnDeadlineCheck_Click(object sender, EventArgs e)
        {
            string table = "collect";
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT collect_is_collected, collect_order_no, collect_billing_date, collect_closing_date, collect_price FROM collect");
            sql.Append(" INNER JOIN orders");
            sql.Append(" ON collect_order_no = orders_orderreceive_no");
            sql.Append(" INNER JOIN orderreceive");
            sql.Append(" ON(orderreceive_no = orders_orderreceive_no");
            sql.Append(" AND orderreceive_serialnumber = orders_orderreceive_serialnumber)");
            sql.Append(" WHERE orderreceive_serialnumber = 1 ");
            sql.Append(" AND  collect_is_collected = 0 ");
            sql.Append(" ORDER BY collect_closing_date ");

            //DB接続
            DatabaseConnector dbco = new DatabaseConnector();
            dataSet = dbco.GetDataSet(sql.ToString(), table);
            dbco.Disconnect();
            SetdataGridView(table);
        }

        private void btnComp_Click(object sender, EventArgs e)
        {
            for (int checkcnt = 0; checkcnt < dataGridView1.Rows.Count; checkcnt++)
            {
                int checkboxTtoF = 0;
                string checkflg = dataGridView1.Rows[checkcnt].Cells[0].Value.ToString();
                string collect_order_no = dataGridView1.Rows[checkcnt].Cells[1].Value.ToString();

                Console.WriteLine("チェックフラグ:" + checkflg);
                Console.WriteLine("番号:" + collect_order_no);

                if (checkflg.Equals("True"))
                {
                    string table = "collect";
                    StringBuilder sql = new StringBuilder();

                    sql.Append(" UPDATE collect");
                    sql.Append(" INNER JOIN customersledger");
                    sql.Append(" ON collect_order_no = customersledger_orders_no");
                    sql.Append(" SET collect_is_collected =1 ,customersledger_is_done = 1");
                    sql.Append(" WHERE collect_order_no =" + collect_order_no + "");

                    DatabaseConnector dbco = new DatabaseConnector();
                    dataSet = dbco.GetDataSet(sql.ToString(), table);
                    dbco.Disconnect();

                }

                DialogMessage.I908CollectComp.ShowMessageBox();

                Console.WriteLine("true:1 false:0    " + checkboxTtoF);
            }
        }

        private void SetdataGridView(string table)
        {
            int checkcnt = 0;

            DataTable dataTable = dataSet.Tables[table];
            dataGridView1.Columns.Clear();

            if (dataTable.Rows.Count != 0)
            {
                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                dataGridView1.Columns.Add(check);
                dataGridView1.DataSource = dataTable;
                dataGridView1.Columns.Remove("collect_is_collected");
                dataGridView1.DataSource = dataTable;

                while (checkcnt < dataTable.Rows.Count)
                {
                    string s = dataTable.Rows[checkcnt][0].ToString();

                    Console.WriteLine(s);

                    if (s.Equals(1.ToString()))
                    {
                        dataGridView1.Rows[checkcnt].Cells[0].Value = true;
                    }
                    else if (s.Equals(0.ToString()))
                    {
                        dataGridView1.Rows[checkcnt].Cells[0].Value = false;
                    }
                    checkcnt++;
                }
                SetdataGridViewProperty();
                dataGridView1.Rows[0].Selected = true;
            }
            else
            {

            }
        }

        private void SetdataGridViewProperty()
        {
            string[] headerText = new string[] { "", "請求番号", "請求日時", "請求締日", "請求金額" };

            for (int count = 0; count < headerText.Length; count++)
            {
                if (Check.IsEmpty(headerText[count]))
                {
                    dataGridView1.Columns[count].ReadOnly = false;
                }
                else
                {
                    dataGridView1.Columns[count].ReadOnly = true;
                }
                dataGridView1.Columns[count].HeaderText = headerText[count];
            }

            int width = dataGridView1.Size.Width;
            int scrollBar = 17;
            if (dataGridView1.Rows.Count > 25)
            {
                width = width - scrollBar;
            }

            dataGridView1.Columns[0].Width = 25;
            dataGridView1.Columns[1].Width = 100;
            dataGridView1.Columns[2].Width = 100;
            dataGridView1.Columns[3].Width = 100;
            dataGridView1.Columns[4].Width = 100;
        }

        private void CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell cell = dataGridView1.CurrentRow.Cells[0] as DataGridViewCheckBoxCell;

            if (cell.Value is true)//現状0件の場合エラーが出る
            {
                dataGridView1.CurrentRow.Cells[0].Value = false;
            }
            else
            {
                dataGridView1.CurrentRow.Cells[0].Value = true;
            }
        }

        protected override void FoundKeyDown(object sender, KeyEventArgs e)
        {
            base.FoundKeyDown(sender, e);
            if (e.KeyCode == Keys.Enter)
            {
                CellDoubleClick(null, null);
            }
        }

        private void checkAllSelect_CheckedChanged(object sender, EventArgs e)
        {
            bool isChecked = checkAllSelect.Checked;
            if (dataGridView1.Rows.Count != 0)
            {
                for (int index = 0; index < dataGridView1.Rows.Count; index++)
                {
                    dataGridView1.Rows[index].Cells[0].Value = isChecked;
                }
            }
        }
    }
}
