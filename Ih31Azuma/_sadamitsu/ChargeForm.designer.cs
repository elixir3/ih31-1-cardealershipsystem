﻿namespace Ih31Azuma
{
    partial class ChargeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.txtYear = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCollectNum = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnDeadlineCheck = new System.Windows.Forms.Button();
            this.checkAllSelect = new System.Windows.Forms.CheckBox();
            this.btnComp = new System.Windows.Forms.Button();
            this.txtMonth = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnF1
            // 
            this.BtnF1.TabIndex = 27;
            // 
            // BtnF2
            // 
            this.BtnF2.TabIndex = 28;
            // 
            // BtnF3
            // 
            this.BtnF3.TabIndex = 29;
            // 
            // BtnF4
            // 
            this.BtnF4.TabIndex = 30;
            // 
            // BtnF5
            // 
            this.BtnF5.TabIndex = 31;
            // 
            // BtnF7
            // 
            this.BtnF7.TabIndex = 33;
            // 
            // LbStatusBar
            // 
            this.LbStatusBar.Size = new System.Drawing.Size(985, 30);
            // 
            // BtnF6
            // 
            this.BtnF6.TabIndex = 32;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(47, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "請求期日";
            // 
            // txtYear
            // 
            this.txtYear.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYear.Location = new System.Drawing.Point(140, 24);
            this.txtYear.MaxLength = 4;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(69, 23);
            this.txtYear.TabIndex = 1;
            this.txtYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FoundKeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(215, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "年";
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(50, 109);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(100, 33);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "絞り込み";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(47, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "請求書番号";
            // 
            // txtCollectNum
            // 
            this.txtCollectNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtCollectNum.Location = new System.Drawing.Point(140, 64);
            this.txtCollectNum.Name = "txtCollectNum";
            this.txtCollectNum.Size = new System.Drawing.Size(110, 23);
            this.txtCollectNum.TabIndex = 3;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(50, 190);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.Size = new System.Drawing.Size(428, 206);
            this.dataGridView1.TabIndex = 7;
            this.dataGridView1.TabStop = false;
            // 
            // btnDeadlineCheck
            // 
            this.btnDeadlineCheck.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnDeadlineCheck.Location = new System.Drawing.Point(377, 109);
            this.btnDeadlineCheck.Name = "btnDeadlineCheck";
            this.btnDeadlineCheck.Size = new System.Drawing.Size(100, 33);
            this.btnDeadlineCheck.TabIndex = 5;
            this.btnDeadlineCheck.Text = "納期確認";
            this.btnDeadlineCheck.UseVisualStyleBackColor = true;
            this.btnDeadlineCheck.Click += new System.EventHandler(this.btnDeadlineCheck_Click);
            // 
            // checkAllSelect
            // 
            this.checkAllSelect.AutoSize = true;
            this.checkAllSelect.Font = new System.Drawing.Font("MS UI Gothic", 12F);
            this.checkAllSelect.Location = new System.Drawing.Point(50, 158);
            this.checkAllSelect.Name = "checkAllSelect";
            this.checkAllSelect.Size = new System.Drawing.Size(99, 20);
            this.checkAllSelect.TabIndex = 6;
            this.checkAllSelect.Text = "すべて選択";
            this.checkAllSelect.UseVisualStyleBackColor = true;
            this.checkAllSelect.CheckedChanged += new System.EventHandler(this.checkAllSelect_CheckedChanged);
            // 
            // btnComp
            // 
            this.btnComp.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnComp.Location = new System.Drawing.Point(377, 414);
            this.btnComp.Name = "btnComp";
            this.btnComp.Size = new System.Drawing.Size(100, 33);
            this.btnComp.TabIndex = 10;
            this.btnComp.Text = "入金完了";
            this.btnComp.UseVisualStyleBackColor = true;
            this.btnComp.Click += new System.EventHandler(this.btnComp_Click);
            // 
            // txtMonth
            // 
            this.txtMonth.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonth.Location = new System.Drawing.Point(250, 24);
            this.txtMonth.MaxLength = 2;
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Size = new System.Drawing.Size(37, 23);
            this.txtMonth.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 12F);
            this.label4.Location = new System.Drawing.Point(293, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 16);
            this.label4.TabIndex = 15;
            this.label4.Text = "月";
            // 
            // ChargeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtMonth);
            this.Controls.Add(this.btnComp);
            this.Controls.Add(this.checkAllSelect);
            this.Controls.Add(this.btnDeadlineCheck);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.txtCollectNum);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtYear);
            this.Controls.Add(this.label2);
            this.Name = "ChargeForm";
            this.Text = "入金管理";
            this.Controls.SetChildIndex(this.BtnF7, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.txtYear, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.btnSearch, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.txtCollectNum, 0);
            this.Controls.SetChildIndex(this.dataGridView1, 0);
            this.Controls.SetChildIndex(this.btnDeadlineCheck, 0);
            this.Controls.SetChildIndex(this.checkAllSelect, 0);
            this.Controls.SetChildIndex(this.btnComp, 0);
            this.Controls.SetChildIndex(this.LbStatusBar, 0);
            this.Controls.SetChildIndex(this.BtnF1, 0);
            this.Controls.SetChildIndex(this.BtnF2, 0);
            this.Controls.SetChildIndex(this.BtnF3, 0);
            this.Controls.SetChildIndex(this.BtnF4, 0);
            this.Controls.SetChildIndex(this.BtnF5, 0);
            this.Controls.SetChildIndex(this.BtnF6, 0);
            this.Controls.SetChildIndex(this.txtMonth, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtYear;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCollectNum;
        private System.Windows.Forms.Button btnDeadlineCheck;
        private System.Windows.Forms.CheckBox checkAllSelect;
        private System.Windows.Forms.Button btnComp;
        private System.Windows.Forms.TextBox txtMonth;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}