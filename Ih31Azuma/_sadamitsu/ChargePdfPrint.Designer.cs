﻿namespace Ih31Azuma
{
    partial class ChargePdfPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChargePdfPrint));
            this.axAcroPDF1 = new AxAcroPDFLib.AxAcroPDF();
            ((System.ComponentModel.ISupportInitialize)(this.axAcroPDF1)).BeginInit();
            this.SuspendLayout();
            // 
            // axAcroPDF1
            // 
            this.axAcroPDF1.Enabled = true;
            this.axAcroPDF1.Location = new System.Drawing.Point(0, 0);
            this.axAcroPDF1.Name = "axAcroPDF1";
            this.axAcroPDF1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axAcroPDF1.OcxState")));
            this.axAcroPDF1.Size = new System.Drawing.Size(987, 480);
            this.axAcroPDF1.TabIndex = 9;
            // 
            // ChargePdfPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.axAcroPDF1);
            this.Name = "ChargePdfPrint";
            this.Text = "ChargePdfPrint";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ChargePdfPrint_FormClosing);
            this.Load += new System.EventHandler(this.ChargePdfPrint_Load);
            this.Controls.SetChildIndex(this.LbStatusBar, 0);
            this.Controls.SetChildIndex(this.BtnF1, 0);
            this.Controls.SetChildIndex(this.BtnF2, 0);
            this.Controls.SetChildIndex(this.BtnF3, 0);
            this.Controls.SetChildIndex(this.BtnF4, 0);
            this.Controls.SetChildIndex(this.BtnF5, 0);
            this.Controls.SetChildIndex(this.BtnF7, 0);
            this.Controls.SetChildIndex(this.BtnF6, 0);
            this.Controls.SetChildIndex(this.axAcroPDF1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.axAcroPDF1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxAcroPDFLib.AxAcroPDF axAcroPDF1;
    }
}