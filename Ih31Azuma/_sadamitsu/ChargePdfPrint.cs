﻿using Ih31Azuma.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ih31Azuma
{
    public partial class ChargePdfPrint : IFormDesign
    {
        public ChargePdfPrint()
        {
            InitializeComponent();
            axAcroPDF1.LoadFile(@"charge/exports.pdf");
           
        }

        private void ChargePdfPrint_FormClosing(object sender, FormClosingEventArgs e)
        {
            PrintPdf.EndPrint("charge");
        }

        private void ChargePdfPrint_Load(object sender, EventArgs e)
        {
            axAcroPDF1.Refresh();
        }
    }
}
