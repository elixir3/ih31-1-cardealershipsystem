﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Ih31Azuma.Database;
using Ih31Azuma.Util;
using Ih31Azuma._sadamitsu;
using System.Collections.Generic;
using static Ih31Azuma._sadamitsu.ChargeCrystalReportPrint;

namespace Ih31Azuma
{
    public partial class CollectForm : IFormDesign
    {
        private DataSet dataSet;

        public CollectForm()
        {
            InitializeComponent();
            EnableDataGridViewOperator();
        }

        public class zip
        {
            public string message { get; set; } = "";
        }

        private void FoundKeyPress(object sender, KeyPressEventArgs e)
        {
            if (Check.IsExceptionKeys(e.KeyChar) || Check.IsNumeric(e.KeyChar.ToString()))
            {
                return;
            }

            e.Handled = true;
        }

        protected override void FoundKeyDown(object sender, KeyEventArgs e)
        {
            base.FoundKeyDown(sender, e);
            if (e.KeyCode == Keys.Enter)
            {
                CellDoubleClick(null, null);
            }
        }

        protected override void SetFunctionKey()
        {
            base.SetFunctionKey();
            EnableButton(BtnF1, "検索 (F1)");
            EnableButton(BtnF2, "印刷 (F2)");
        }


        //検索
        protected override void BtnF1_Click(object sender, EventArgs e)
        {
            base.BtnF1_Click(sender, e);
            btnSearch_Click(null, null);
        }

        //印刷
        protected override void BtnF2_Click(object sender, EventArgs e)
        {
            base.BtnF2_Click(sender, e);
            btnPrint_Click(null, null);
        }

        //納期確認
        private void btnDeadlineCheck_Click(object sender, EventArgs e)
        {
            string table = "collect";
            StringBuilder sql = new StringBuilder();

            sql.Append("SELECT collect_no,customers_name,orderreceive_carname,purchase_price,purchase_expenses,collect_tesuryo,purchase_price+purchase_expenses+collect_tesuryo AS sum_price FROM collect");
            sql.Append(" INNER JOIN orders");
            sql.Append(" ON collect_order_no = orders_orderreceive_no");
            sql.Append(" INNER JOIN orderreceive");
            sql.Append(" ON orderreceive_no = orders_orderreceive_no");
            sql.Append(" AND orderreceive_serialnumber = orders_orderreceive_serialnumber");
            sql.Append(" INNER JOIN purchase");
            sql.Append(" ON orderreceive_no = purchase_orderreceive_no");
            sql.Append(" AND orderreceive_serialnumber = purchase_orderreceive_serialnumber");
            sql.Append(" INNER JOIN customers");
            sql.Append(" ON orderreceive_client_no = customers_no");
            sql.Append(" where orderreceive_serialnumber = 1 ");

            //DB接続
            DatabaseConnector dbco = new DatabaseConnector();
            dataSet = dbco.GetDataSet(sql.ToString(), table);
            dbco.Disconnect();
            SetdataGridView(table);
        }

        //検索
        private void btnSearch_Click(object sender, EventArgs e)
        {
            string table = "collect";
            StringBuilder sql = new StringBuilder();

            sql.Append("SELECT collect_no,customers_name,orderreceive_carname,purchase_price,purchase_expenses,collect_tesuryo,purchase_price+purchase_expenses+collect_tesuryo AS sum_price FROM collect");
            sql.Append(" INNER JOIN orders");
            sql.Append(" ON collect_order_no = orders_orderreceive_no");
            sql.Append(" INNER JOIN orderreceive");
            sql.Append(" ON orderreceive_no = orders_orderreceive_no");
            sql.Append(" AND orderreceive_serialnumber = orders_orderreceive_serialnumber");
            sql.Append(" INNER JOIN purchase");
            sql.Append(" ON orderreceive_no = purchase_orderreceive_no");
            sql.Append(" AND orderreceive_serialnumber = purchase_orderreceive_serialnumber");
            sql.Append(" INNER JOIN customers");
            sql.Append(" ON orderreceive_client_no = customers_no");
            sql.Append(" where orderreceive_serialnumber = 1 ");

            if (!txtYear.Text.Equals("") && !txtMonth.Text.Equals("") && !txtCollectNum.Text.Equals(""))
            {
                sql.Append(" AND collect_closing_date like '%" + txtYear.Text + "" + "/" + "" + txtMonth.Text + "%'");
                sql.Append(" AND collect_order_no = '" + txtCollectNum.Text + "'");
            }
            else if (!txtYear.Text.Equals("") && !txtMonth.Text.Equals(""))
            {
                sql.Append(" AND  collect_closing_date like '%" + txtYear.Text + "" + "/" + "" + txtMonth.Text + "%'");
            }
            else if (!txtCollectNum.Text.Equals(""))
            {
                sql.Append(" AND collect_order_no = '" + txtCollectNum.Text + "'");
            }

            DatabaseConnector dbco = new DatabaseConnector();
            dataSet = dbco.GetDataSet(sql.ToString(), table);
            dbco.Disconnect();
            SetdataGridView(table);
        }

        //請求書番号に変更
        public static string ConvetCollecttNo(string s)
        {
            if (!int.TryParse(s, out int no))
            {
                Error();
            }

            if (s.Length <= 5)
            {
                return (900000 + no).ToString();
            }
            else if (s.Length == 6 && s.Substring(0, 1) == "9")
            {
                return s;
            }

            Error();

            return null;
        }
        private static void Error()
        {
            throw new FormatException();
        }

        //印刷
        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count == 0)
            {
                return;
            }

            DatabaseConnector conn = new DatabaseConnector();
            int count = dataGridView1.Rows.Count;
            int printcnt = 0;
            DataSet printData = null;
            ChangeDocumentDetail documentDetail = null;
            ChargeCrystalReportPrint reportForm = null;

            for (int checkcnt = 0; checkcnt < count; checkcnt++)
            {
                if (dataGridView1.Rows[checkcnt].Cells[0].Value is true)
                {
                    if (printcnt != 0)
                    {
                        reportForm = new ChargeCrystalReportPrint(printData, documentDetail, (printcnt - 1));
                    }

                    documentDetail = new ChangeDocumentDetail();

                    documentDetail.CollectNum = dataGridView1.Rows[checkcnt].Cells[1].Value.ToString();

                    string table = "collect_data";
                    StringBuilder sql = new StringBuilder();

                    sql.Append("SELECT collect_order_no,orderreceive_carname,purchase_price,purchase_expenses,collect_tesuryo,cast(FLOOR(purchase_price+purchase_expenses+collect_tesuryo) AS char) AS sum_price from collect_data");
                    sql.Append(" where collect_order_no = " + Convert.ToInt32(documentDetail.CollectNum) % 900000);
                    DataSet dataSet = conn.GetDataSet(sql.ToString(), table);

                    documentDetail.CollectNum = ConvetCollecttNo(dataGridView1.Rows[checkcnt].Cells[1].Value.ToString());
                    documentDetail.CustomerName = dataGridView1.Rows[checkcnt].Cells[2].Value.ToString() + "　　　御中";


                    //Updateに変えて、collect_billing_dateを印刷した時点の時間を取得する。
                    string collectTable = "collect";
                    StringBuilder collectSql = new StringBuilder();
                    string nowDate = DateTime.Today.ToString("yyyy/MM/dd");//現在時間

                    collectSql.Append("update collect Set collect_billing_date= '" + nowDate + "'");
                    collectSql.Append(" where collect_no = '" + documentDetail.CollectNum + "'");

                    DataSet dataSet2 = conn.GetDataSet(collectSql.ToString(), collectTable);

                    string sumCollectTable = "collect";
                    StringBuilder sumCollectSql = new StringBuilder();

                    sumCollectSql.Append("SELECT cast(FLOOR(purchase_price*1.08)+purchase_expenses+collect_tesuryo AS char) AS sum_TaxPrice from orders");
                    sumCollectSql.Append(" INNER JOIN collect");
                    sumCollectSql.Append(" ON collect_order_no = orders_orderreceive_no");
                    sumCollectSql.Append(" INNER JOIN orderreceive");
                    sumCollectSql.Append(" ON orderreceive_no = orders_orderreceive_no");
                    sumCollectSql.Append(" AND orderreceive_serialnumber = orders_orderreceive_serialnumber");
                    sumCollectSql.Append(" INNER JOIN purchase");
                    sumCollectSql.Append(" ON orderreceive_no = purchase_orderreceive_no");
                    sumCollectSql.Append(" AND orderreceive_serialnumber = purchase_orderreceive_serialnumber");

                    sumCollectSql.Append(" where collect_no = " + documentDetail.CollectNum);

                    DataSet dataSet3 = conn.GetDataSet(sumCollectSql.ToString(), sumCollectTable);

                    string price = dataSet.Tables[table].Rows[0][5].ToString();
                    price = MyConvert.AddComma(price, true);
                    Console.WriteLine(price);
                    dataSet.Tables[table].Rows[0][5] = price;


                    documentDetail.CollectBillingDate = nowDate;
                    documentDetail.Total = dataSet.Tables[table].Rows[0][5].ToString();
                    documentDetail.TaxTotal = MyConvert.AddComma(dataSet3.Tables[collectTable].Rows[0][0].ToString(), true);

                    printData = dataSet.Copy();
                    printcnt++;
                }
            }

            if (printData == null)
            {
                return;
            }

            if (printcnt == 1)
            {
                reportForm = new ChargeCrystalReportPrint(printData, documentDetail);
                reportForm.Show();
            }
            else
            {
                reportForm = new ChargeCrystalReportPrint(printData, documentDetail, (printcnt - 1));
                reportForm.Dispose();

                PrintPdf.JoinPdf(printcnt++, "charge/");
                ChargePdfPrint form = new ChargePdfPrint();
                form.Show();
            }

            conn.Disconnect();
            printcnt = 0;
        }

        private void SetdataGridView(string table)
        {
            DataTable dataTable = dataSet.Tables[table];
            dataGridView1.Columns.Clear();


            if (dataTable.Rows.Count != 0)
            {
                DataGridViewCheckBoxColumn check = new DataGridViewCheckBoxColumn();
                dataGridView1.Columns.Add(check);
                dataGridView1.DataSource = dataTable;
                SetdataGridViewProperty();
                dataGridView1.Rows[0].Selected = true;
            }
            else
            {

            }
        }

        //DataGridViewの表示
        private void SetdataGridViewProperty()
        {
            string[] headerText = new string[] { "", "請求番号", "顧客名", "車名", "単価", "経費", "手数料", "合計金額" };
            for (int count = 0; count < headerText.Length; count++)
            {
                if (Check.IsEmpty(headerText[count]))
                {
                    dataGridView1.Columns[count].ReadOnly = false;
                }
                else
                {
                    dataGridView1.Columns[count].ReadOnly = true;
                }
                dataGridView1.Columns[count].HeaderText = headerText[count];
            }

            int width = dataGridView1.Size.Width;
            int scrollBar = 17;
            if (dataGridView1.Rows.Count > 25)
            {
                width = width - scrollBar;
            }

            dataGridView1.Columns[0].Width = 25;
            dataGridView1.Columns[1].Width = 100;
            dataGridView1.Columns[2].Width = 150;
            dataGridView1.Columns[3].Width = 260;
            dataGridView1.Columns[4].Width = 80;
            dataGridView1.Columns[5].Width = 80;
            dataGridView1.Columns[6].Width = 80;

            SetSelectedRow();
        }

        private void SetSelectedRow()
        {
            if (dataGridView1.Rows.Count == 0)
            {
                return;
            }

            dataGridView1.Rows[0].Selected = true;
          //  dataGridView1.CurrentCell = dataGridView1.Rows[0].Cells[0];
        }

        private void CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Rows.Count == 0)
            {
                return;
            }

            DataGridViewCheckBoxCell cell = dataGridView1.CurrentRow.Cells[0] as DataGridViewCheckBoxCell;

            if (cell.Value is true)//現状0件の場合エラーが出る
            {
                dataGridView1.CurrentRow.Cells[0].Value = false;
            }
            else
            {
                dataGridView1.CurrentRow.Cells[0].Value = true;
            }

            //MoveSelectRow(Keys.F4, dataGridView1);
        }

        protected override void BtnF3_Click(object sender = null, EventArgs e = null)
        {
           // base.BtnF3_Click(sender, e);
            MoveSelectRow(Keys.F3, dataGridView1);
        }

        protected override void BtnF4_Click(object sender = null, EventArgs e = null)
        {
            //base.BtnF4_Click(sender, e);
            MoveSelectRow(Keys.F4, dataGridView1);
        }

        //全チェック
        private void checkAllSelect_CheckedChanged(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count == 0)
            {
                return;
            }

            bool isChecked = checkAllSelect.Checked;
            if (dataGridView1.Rows.Count != 0)
            {
                for (int index = 0; index < dataGridView1.Rows.Count; index++)
                {
                    dataGridView1.Rows[index].Cells[0].Value = isChecked;
                }
            }
        }
    }
}
