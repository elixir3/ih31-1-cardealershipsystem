-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2017 年 12 朁E12 日 00:54
-- サーバのバージョン： 10.1.21-MariaDB
-- PHP Version: 5.6.30
DROP DATABASE IF EXISTS `sugukuru`;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sugukuru`
--
CREATE DATABASE IF NOT EXISTS `sugukuru` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `sugukuru`;

-- --------------------------------------------------------

--
-- テーブルの構造 `collect`
--
-- 作成日時： 2017 年 12 朁E07 日 13:01
--

CREATE TABLE IF NOT EXISTS `collect` (
  `collect_no` int(6) NOT NULL,
  `collect_order_no` int(5) UNSIGNED ZEROFILL NOT NULL,
  `collect_billing_date` char(10) DEFAULT NULL,
  `collect_closing_date` char(10) NOT NULL,
  `collect_price` int(9) NOT NULL,
  `collect_is_collected` int(1) DEFAULT '0',
  `collect_insert_date` date NOT NULL,
  `collect_insert_time` time NOT NULL,
  `collect_update_date` date DEFAULT NULL,
  `collect_update_time` time DEFAULT NULL,
  `collect_tesuryo` int(10) DEFAULT '20000',
  PRIMARY KEY (`collect_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `collect`
--

INSERT INTO `collect` (`collect_no`, `collect_order_no`, `collect_billing_date`, `collect_closing_date`, `collect_price`, `collect_is_collected`, `collect_insert_date`, `collect_insert_time`, `collect_update_date`, `collect_update_time`, `collect_tesuryo`) VALUES
(900001, 00001, '2017/12/11', '2017/12/25', 6706800, 1, '2017-12-07', '01:31:36', NULL, NULL, 20000),
(900002, 00002, '2017/12/11', '2017/12/25', 3245400, 1, '2017-12-09', '15:46:22', NULL, NULL, 20000),
(900003, 00003, '2017/12/11', '2017/12/25', 6501600, 1, '2017-12-07', '20:04:38', NULL, NULL, 20000),
(900004, 00004, '2017/12/11', '2017/12/25', 1306000, 1, '2017-12-09', '23:27:54', NULL, NULL, 20000),
(900005, 00005, '2017/12/11', '2017/12/25', 928000, 1, '2017-12-10', '04:01:44', NULL, NULL, 20000),
(900006, 00006, '2017/12/11', '2017/12/25', 1100000, 1, '2017-12-11', '19:50:43', NULL, NULL, 20000);

-- --------------------------------------------------------

--
-- ビュー用の代替構造 `collect_data`
-- (See below for the actual view)
--
CREATE TABLE IF NOT EXISTS `collect_data` (
`collect_order_no` int(5) unsigned zerofill
,`orderreceive_carname` varchar(50)
,`purchase_price` int(10)
,`purchase_expenses` int(10)
,`collect_tesuryo` int(10)
,`sum_price` bigint(13)
);

-- --------------------------------------------------------

--
-- テーブルの構造 `customers`
--
-- 作成日時： 2017 年 12 朁E04 日 05:30
--

CREATE TABLE IF NOT EXISTS `customers` (
  `customers_no` int(5) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `customers_name` varchar(20) NOT NULL,
  `customers_zipcode` varchar(7) DEFAULT NULL,
  `customers_pref` varchar(4) DEFAULT NULL,
  `customers_address` varchar(20) DEFAULT NULL,
  `customers_tel` varchar(15) DEFAULT NULL,
  `customers_bank` varchar(30) DEFAULT NULL,
  `customers_account` varchar(20) DEFAULT NULL,
  `customers_account_number` varchar(13) NOT NULL DEFAULT '012344  14789',
  `customers_creditlimit` int(10) DEFAULT NULL,
  `customers_from` varchar(10) DEFAULT NULL,
  `customers_to` varchar(10) DEFAULT NULL,
  `customers_insert_date` date NOT NULL,
  `customers_insert_time` time NOT NULL,
  `customers_update_date` date DEFAULT NULL,
  `customer_update_time` time DEFAULT NULL,
  PRIMARY KEY (`customers_no`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `customers`
--

INSERT INTO `customers` (`customers_no`, `customers_name`, `customers_zipcode`, `customers_pref`, `customers_address`, `customers_tel`, `customers_bank`, `customers_account`, `customers_account_number`, `customers_creditlimit`, `customers_from`, `customers_to`, `customers_insert_date`, `customers_insert_time`, `customers_update_date`, `customer_update_time`) VALUES
(00001, '株式会社アカマツ', '5900123', '大阪府', '梅田0089', '0120-333-906', '三井住友銀行', '(株)アカマツ', '012344  14789', 500000000, '17/11/27', '', '2017-11-27', '10:48:09', '0000-00-00', '00:00:00'),
(00002, '株式会社かわしま', '5660022', '大阪府', '摂津市三島１丁目１−１', '0656784567', '三井住友', '株式会社川島', '012344  14789', 40000000, NULL, NULL, '2017-12-06', '22:25:38', NULL, NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `customersledger`
--
-- 作成日時： 2017 年 12 朁E06 日 13:12
--

CREATE TABLE IF NOT EXISTS `customersledger` (
  `customersledger_no` int(5) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `customersledger_orders_no` int(5) UNSIGNED ZEROFILL NOT NULL,
  `customersledger_accountsreceivable` int(10) NOT NULL,
  `customersledger_insert_date` date NOT NULL,
  `customersledger_insert_time` time NOT NULL,
  `customersledger_update_date` date DEFAULT NULL,
  `customersledger_update_time` time DEFAULT NULL,
  `customersledger_client_no` int(5) UNSIGNED ZEROFILL NOT NULL,
  `customersledger_is_done` int(1) DEFAULT '0',
  PRIMARY KEY (`customersledger_no`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- テーブルのデータのダンプ `customersledger`
--

INSERT INTO `customersledger` (`customersledger_no`, `customersledger_orders_no`, `customersledger_accountsreceivable`, `customersledger_insert_date`, `customersledger_insert_time`, `customersledger_update_date`, `customersledger_update_time`, `customersledger_client_no`, `customersledger_is_done`) VALUES
(00001, 00001, 6200000, '2017-12-03', '16:10:38', NULL, NULL, 00001, 1),
(00002, 00002, 3000000, '2017-12-03', '16:14:41', NULL, NULL, 00001, 1),
(00003, 00003, 6020000, '0000-00-00', '00:00:00', NULL, NULL, 00002, 1),
(00004, 00004, 1210000, '2017-12-09', '23:21:53', NULL, NULL, 00002, 1),
(00005, 00005, 900000, '2017-12-10', '03:53:32', NULL, NULL, 00002, 0),
(00006, 00006, 1020000, '2017-12-10', '23:39:45', NULL, NULL, 00001, 1),
(00007, 00007, 1200000, '2017-12-12', '00:45:27', NULL, NULL, 00001, 0);

-- --------------------------------------------------------

--
-- テーブルの構造 `employees`
--
-- 作成日時： 2017 年 12 朁E04 日 05:30
--

CREATE TABLE IF NOT EXISTS `employees` (
  `Employees_no` int(5) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `Employees_name` varchar(20) NOT NULL,
  `Employees_dapartment` tinyint(1) NOT NULL,
  `Employees_password` varchar(64) NOT NULL DEFAULT '6AF847986D7204F45EDE90B2956A242BDAD4F0165D0918A4F37EC0643FCCF6C1',
  `Employees_insert_date` date NOT NULL,
  `Employees_insert_time` time NOT NULL,
  `Employees_update_date` date DEFAULT NULL,
  `Employees_update_time` time DEFAULT NULL,
  PRIMARY KEY (`Employees_no`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `employees`
--

INSERT INTO `employees` (`Employees_no`, `Employees_name`, `Employees_dapartment`, `Employees_password`, `Employees_insert_date`, `Employees_insert_time`, `Employees_update_date`, `Employees_update_time`) VALUES
(00001, '伊藤　詩織', 0, '6AF847986D7204F45EDE90B2956A242BDAD4F0165D0918A4F37EC0643FCCF6C1', '2017-11-15', '14:22:00', '0000-00-00', '00:00:00'),
(00002, '石田　一', 1, '6AF847986D7204F45EDE90B2956A242BDAD4F0165D0918A4F37EC0643FCCF6C1', '2017-11-15', '09:43:39', '0000-00-00', '00:00:00'),
(00003, '上田　祐樹', 2, '6AF847986D7204F45EDE90B2956A242BDAD4F0165D0918A4F37EC0643FCCF6C1', '2017-11-08', '11:28:28', '0000-00-00', '00:00:00'),
(00004, '若鷺　辰巳', 1, '6AF847986D7204F45EDE90B2956A242BDAD4F0165D0918A4F37EC0643FCCF6C1', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00');

-- --------------------------------------------------------

--
-- テーブルの構造 `orderreceive`
--
-- 作成日時： 2017 年 12 朁E09 日 05:45
--

CREATE TABLE IF NOT EXISTS `orderreceive` (
  `orderreceive_no` int(5) UNSIGNED ZEROFILL NOT NULL,
  `orderreceive_serialnumber` int(5) NOT NULL,
  `orderreceive_carname` varchar(50) DEFAULT NULL,
  `orderreceive_modelyear` varchar(3) DEFAULT NULL,
  `orderreceive_remarks` varchar(70) DEFAULT NULL,
  `orderreceive_deliverydate` varchar(20) DEFAULT '',
  `orderreceive_insert_date` date NOT NULL,
  `orderreceive_insert_time` time NOT NULL,
  `orderreceive_update_date` date DEFAULT NULL,
  `orderreceive_update_time` time DEFAULT NULL,
  `orderreceive_client_no` int(5) UNSIGNED ZEROFILL NOT NULL,
  `orderreceive_status` int(1) DEFAULT '0',
  PRIMARY KEY (`orderreceive_no`,`orderreceive_serialnumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- テーブルのデータのダンプ `orderreceive`
--

INSERT INTO `orderreceive` (`orderreceive_no`, `orderreceive_serialnumber`, `orderreceive_carname`, `orderreceive_modelyear`, `orderreceive_remarks`, `orderreceive_deliverydate`, `orderreceive_insert_date`, `orderreceive_insert_time`, `orderreceive_update_date`, `orderreceive_update_time`, `orderreceive_client_no`, `orderreceive_status`) VALUES
(00001, 1, 'トヨタ　カローラフィールダー　ハイブリッドＧ', 'H27', 'Ｗ×Ｂ入荷純正ナビ\r\nバックカメラ ＨＩＤ', NULL, '2017-12-03', '15:46:44', NULL, NULL, 00001, 1),
(00001, 2, 'フォード　マスタング　2.3エコブースト', 'H28', '黒', '', '2017-12-03', '15:58:37', NULL, NULL, 00001, 1),
(00002, 1, 'ニッサン　エルグランド　2.5 250ハイウェイスターS', 'H29', NULL, '', '2017-12-03', '16:18:55', NULL, NULL, 00001, 4),
(00003, 1, 'メルセデス・ベンツ C', 'H27', '', '', '2017-12-07', '19:54:15', NULL, NULL, 00002, 0),
(00003, 2, 'メルセデス・ベンツ C', 'H27', '', '', '2017-12-07', '19:54:15', NULL, NULL, 00002, 0),
(00004, 1, 'ホンダ フィット H23', 'H23', '', '2017/12/12', '2017-12-09', '23:21:53', NULL, NULL, 00002, 4),
(00005, 1, 'ニッサン　マーチ', 'H23', '', '2017/12/10', '2017-12-10', '03:53:32', NULL, NULL, 00002, 1),
(00006, 1, 'フォルクスワーゲン　ゴルフ', 'H23', '', '2017/12/21', '2017-12-10', '23:39:45', NULL, NULL, 00001, 4),
(00007, 1, 'シボレー カマロ', 'H23', '', '', '2017-12-12', '00:45:27', NULL, NULL, 00001, 0);

-- --------------------------------------------------------

--
-- ビュー用の代替構造 `orderreceive_data`
-- (See below for the actual view)
--
CREATE TABLE IF NOT EXISTS `orderreceive_data` (
`orderreceive_no` int(5) unsigned zerofill
,`orderreceive_serialnumber` int(5)
,`orderreceive_carname` varchar(50)
,`orderreceive_modelyear` varchar(3)
,`orderreceive_remarks` varchar(70)
,`orders_price` int(10)
);

-- --------------------------------------------------------

--
-- テーブルの構造 `orders`
--
-- 作成日時： 2017 年 12 朁E04 日 05:30
--

CREATE TABLE IF NOT EXISTS `orders` (
  `orders_no` int(6) UNSIGNED ZEROFILL NOT NULL,
  `orders_price` int(10) NOT NULL,
  `orders_insert_date` date NOT NULL,
  `orders_insert_time` time NOT NULL,
  `orders_update_date` date DEFAULT NULL,
  `orders_update_time` time DEFAULT NULL,
  `orders_orderreceive_no` int(5) UNSIGNED ZEROFILL NOT NULL,
  `orders_orderreceive_serialnumber` int(5) NOT NULL,
  PRIMARY KEY (`orders_no`,`orders_orderreceive_serialnumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `orders`
--

INSERT INTO `orders` (`orders_no`, `orders_price`, `orders_insert_date`, `orders_insert_time`, `orders_update_date`, `orders_update_time`, `orders_orderreceive_no`, `orders_orderreceive_serialnumber`) VALUES
(100001, 1600000, '2017-12-03', '15:59:53', NULL, NULL, 00001, 1),
(100001, 4600000, '2017-12-03', '16:02:53', NULL, NULL, 00001, 2),
(100002, 3000000, '2017-12-03', '16:19:52', NULL, NULL, 00002, 1),
(100003, 3000000, '2017-12-07', '19:54:15', NULL, NULL, 00003, 1),
(100003, 3000000, '2017-12-07', '19:54:15', NULL, NULL, 00003, 2),
(100004, 0, '2017-12-09', '23:21:53', NULL, NULL, 00004, 1),
(100005, 900000, '2017-12-10', '03:53:32', NULL, NULL, 00005, 1),
(100006, 1200000, '2017-12-10', '23:39:45', NULL, NULL, 00006, 1),
(100007, 1200000, '2017-12-12', '00:45:27', NULL, NULL, 00007, 1);

-- --------------------------------------------------------

--
-- テーブルの構造 `purchase`
--
-- 作成日時： 2017 年 12 朁E07 日 16:27
--

CREATE TABLE IF NOT EXISTS `purchase` (
  `purchase_no` int(6) NOT NULL,
  `purchase_date` varchar(10) DEFAULT NULL,
  `purchase_price` int(10) DEFAULT NULL,
  `purchase_expenses` int(10) DEFAULT NULL,
  `purchase_car_number` varchar(20) DEFAULT NULL,
  `purchase_orderreceive_no` int(5) UNSIGNED ZEROFILL NOT NULL,
  `purchase_orderreceive_serialnumber` int(5) NOT NULL,
  `purchase_insert_date` date NOT NULL,
  `purchase_insert_time` time NOT NULL,
  `purchase_update_date` date DEFAULT NULL,
  `purchase_update_time` time DEFAULT NULL,
  PRIMARY KEY (`purchase_no`,`purchase_orderreceive_serialnumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `purchase`
--

INSERT INTO `purchase` (`purchase_no`, `purchase_date`, `purchase_price`, `purchase_expenses`, `purchase_car_number`, `purchase_orderreceive_no`, `purchase_orderreceive_serialnumber`, `purchase_insert_date`, `purchase_insert_time`, `purchase_update_date`, `purchase_update_time`) VALUES
(300001, '2017/12/08', 3000000, 10, 'g', 00001, 1, '2017-12-03', '16:39:05', NULL, NULL),
(300001, '2017/12/08', 3000000, 0, '4567890', 00001, 2, '2017-12-04', '13:27:31', NULL, NULL),
(300002, '2017/12/03', 3000000, 5000, 'JJU-999A', 00002, 1, '2017-12-04', '13:31:06', NULL, NULL),
(300003, '2017/12/08', 3000000, 10000, '56789', 00003, 1, '0000-00-00', '00:00:00', NULL, NULL),
(300003, '2017/12/07', 3000000, 10000, '45676567', 00003, 2, '0000-00-00', '00:00:00', NULL, NULL),
(300004, '2017/12/09', 1200000, 10000, '4ryukftui', 00004, 1, '2017-12-09', '23:26:20', NULL, NULL),
(300005, '2017/', 850000, 10000, 'yyy7', 00005, 1, '2017-12-10', '03:55:15', NULL, NULL),
(300006, '2017/12/12', 1000000, 20000, 'AAA777', 00006, 1, '2017-12-10', '23:41:20', NULL, NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `reservecar`
--
-- 作成日時： 2017 年 12 朁E04 日 05:30
--

CREATE TABLE IF NOT EXISTS `reservecar` (
  `reservecar_no` int(2) NOT NULL,
  `reservecar_state` int(1) NOT NULL,
  `reservecar_day` varchar(10) NOT NULL,
  `reservecar_employee_no` int(5) NOT NULL,
  PRIMARY KEY (`reservecar_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- ビュー用の代替構造 `shipment_data`
-- (See below for the actual view)
--
CREATE TABLE IF NOT EXISTS `shipment_data` (
`orderreceive_no` int(5) unsigned zerofill
,`orderreceive_serialnumber` int(5)
,`orderreceive_carname` varchar(50)
,`purchase_car_number` varchar(20)
,`purchase_price` int(10)
,`purchase_expenses` int(10)
);

-- --------------------------------------------------------

--
-- テーブルの構造 `shipping`
--
-- 作成日時： 2017 年 12 朁E04 日 06:02
--

CREATE TABLE IF NOT EXISTS `shipping` (
  `shipping_no` int(6) NOT NULL,
  `shipping_orders_no` int(5) UNSIGNED ZEROFILL NOT NULL,
  `shipping_making_date` varchar(10) DEFAULT NULL,
  `shipping_insert_date` date NOT NULL,
  `shipping_insert_time` time NOT NULL,
  PRIMARY KEY (`shipping_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `shipping`
--

INSERT INTO `shipping` (`shipping_no`, `shipping_orders_no`, `shipping_making_date`, `shipping_insert_date`, `shipping_insert_time`) VALUES
(400001, 00001, '2017/12/07', '2017-12-04', '14:26:13'),
(400002, 00002, '2017/12/09', '0000-00-00', '00:00:00'),
(400003, 00003, '2017/12/07', '0000-00-00', '00:00:00'),
(400004, 00004, '2017/12/09', '0000-00-00', '00:00:00'),
(400005, 00005, '2017/12/10', '0000-00-00', '00:00:00'),
(400006, 00006, '2017/12/11', '0000-00-00', '00:00:00');

-- --------------------------------------------------------

--
-- ビュー用の構造 `collect_data`
--
DROP TABLE IF EXISTS `collect_data`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `collect_data`  AS  select `collect`.`collect_order_no` AS `collect_order_no`,`orderreceive`.`orderreceive_carname` AS `orderreceive_carname`,`purchase`.`purchase_price` AS `purchase_price`,`purchase`.`purchase_expenses` AS `purchase_expenses`,`collect`.`collect_tesuryo` AS `collect_tesuryo`,((`orders`.`orders_price` + `purchase`.`purchase_expenses`) + `collect`.`collect_tesuryo`) AS `sum_price` from (((`collect` join `orders` on((`collect`.`collect_order_no` = `orders`.`orders_orderreceive_no`))) join `orderreceive` on(((`orders`.`orders_orderreceive_no` = `orderreceive`.`orderreceive_no`) and (`orderreceive`.`orderreceive_serialnumber` = `orders`.`orders_orderreceive_serialnumber`)))) join `purchase` on(((`orderreceive`.`orderreceive_no` = `purchase`.`purchase_orderreceive_no`) and (`orderreceive`.`orderreceive_serialnumber` = `purchase`.`purchase_orderreceive_serialnumber`)))) ;

-- --------------------------------------------------------

--
-- ビュー用の構造 `orderreceive_data`
--
DROP TABLE IF EXISTS `orderreceive_data`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `orderreceive_data`  AS  select `orderreceive`.`orderreceive_no` AS `orderreceive_no`,`orderreceive`.`orderreceive_serialnumber` AS `orderreceive_serialnumber`,`orderreceive`.`orderreceive_carname` AS `orderreceive_carname`,`orderreceive`.`orderreceive_modelyear` AS `orderreceive_modelyear`,`orderreceive`.`orderreceive_remarks` AS `orderreceive_remarks`,`orders`.`orders_price` AS `orders_price` from (`orderreceive` join `orders` on(((`orderreceive`.`orderreceive_no` = `orders`.`orders_orderreceive_no`) and (`orderreceive`.`orderreceive_serialnumber` = `orders`.`orders_orderreceive_serialnumber`)))) ;

-- --------------------------------------------------------

--
-- ビュー用の構造 `shipment_data`
--
DROP TABLE IF EXISTS `shipment_data`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `shipment_data`  AS  select `orderreceive`.`orderreceive_no` AS `orderreceive_no`,`orderreceive`.`orderreceive_serialnumber` AS `orderreceive_serialnumber`,`orderreceive`.`orderreceive_carname` AS `orderreceive_carname`,`shipment_data`.`purchase_car_number` AS `purchase_car_number`,`shipment_data`.`purchase_price` AS `purchase_price`,`shipment_data`.`purchase_expenses` AS `purchase_expenses` from (`purchase` `shipment_data` join `orderreceive` on(((`orderreceive`.`orderreceive_no` = `shipment_data`.`purchase_orderreceive_no`) and (`orderreceive`.`orderreceive_serialnumber` = `shipment_data`.`purchase_orderreceive_serialnumber`)))) ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
